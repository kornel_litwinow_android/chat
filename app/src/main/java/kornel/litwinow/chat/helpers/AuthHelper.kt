package kornel.litwinow.chat.helpers

import com.google.firebase.auth.FirebaseAuth

object AuthHelper {
     var auth: FirebaseAuth? = null

    fun getUserAuth() {
        auth = FirebaseAuth.getInstance()
    }

}