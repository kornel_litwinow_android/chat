package kornel.litwinow.chat.notifications

data class DataV2(val user: String = "", val icon: Int = 0, val body: String = "", val title: String = "", val sented: String = "")
