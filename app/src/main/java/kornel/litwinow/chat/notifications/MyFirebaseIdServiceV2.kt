package kornel.litwinow.chat.notifications

import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.FirebaseInstanceIdService
import kornel.litwinow.chat.helpers.ConstHelper
import kornel.litwinow.chat.helpers.AuthHelper

class MyFirebaseIdServiceV2 : FirebaseInstanceIdService() {
    override fun onTokenRefresh() {
        super.onTokenRefresh()
        val refreshToken = FirebaseInstanceId.getInstance().token
        if (AuthHelper.auth?.currentUser != null) {
            updateToken(refreshToken)
        }
    }

    private fun updateToken(refreshToken: String?) {
        val reference = FirebaseDatabase.getInstance().getReference(ConstHelper.TOKENS)
        val token = TokenV2(refreshToken.toString())
        if (AuthHelper.auth?.currentUser != null) {
            reference.child(AuthHelper.auth?.currentUser?.uid.toString()).setValue(token)
        }
    }
}
