package kornel.litwinow.chat.notifications

data class SenderV2(val data: DataV2 = DataV2(), val to: String = "")