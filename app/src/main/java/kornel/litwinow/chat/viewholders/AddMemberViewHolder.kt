package kornel.litwinow.chat.viewholders

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestManager
import kornel.litwinow.chat.R
import kornel.litwinow.chat.model.User
import kotlinx.android.synthetic.main.add_remove_member_item.view.*

class AddMemberViewHolder(val glide: RequestManager, val view: View) : RecyclerView.ViewHolder(view) {
    fun bind(user: User?, listener: AddRemoveMemberListener?) {
        if (user?.imageURL == "default") {
            itemView.profile_image.setImageResource(R.mipmap.ic_person_placeholder)
        } else {
            glide.load(user?.imageURL).fitCenter().into(itemView.profile_image)
        }
        itemView.username.text = view.context.getString(R.string.profile_name_lastname, user?.username, user?.lastname)
        itemView.check_add_remove.setOnClickListener {
            user?.id?.let { it1 -> listener?.addOrRemoveMember(it1) }
        }
    }


    interface AddRemoveMemberListener {
        fun addOrRemoveMember(userId: String)
    }
}