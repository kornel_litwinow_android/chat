package kornel.litwinow.chat.viewholders


import android.view.View
import android.view.View.*
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestManager
import kornel.litwinow.chat.model.Chat
import kornel.litwinow.chat.R
import kornel.litwinow.chat.helpers.ConstHelper
import kornel.litwinow.chat.helpers.DateTimeHelper
import kotlinx.android.synthetic.main.chat_item_right.view.profile_image
import kotlinx.android.synthetic.main.chat_item_right.view.root
import kotlinx.android.synthetic.main.chat_item_right.view.root_img
import kotlinx.android.synthetic.main.chat_item_right.view.root_voice_record
import kotlinx.android.synthetic.main.chat_item_right.view.show_message
import kotlinx.android.synthetic.main.chat_item_right.view.txt_seen
import kotlinx.android.synthetic.main.item_image_message.view.*
import kotlinx.android.synthetic.main.item_voice_record_message.view.*


class MessageRightV2ViewHolder(val glide: RequestManager, val view: View) : RecyclerView.ViewHolder(view) {
    fun bind(chat: Chat?, imageurl: String?, position: Int?, sizeListMessages: Int) {

        if (imageurl != null && imageurl == "default") {
            itemView.profile_image.setImageResource(R.mipmap.ic_person_placeholder)
        } else {
            glide.load(imageurl).fitCenter().into(itemView.profile_image)
        }
        if (chat?.messageType == ConstHelper.TEXT) {
            itemView.show_message.text = chat.message
            itemView.txt_seen.text = DateTimeHelper.getFormattedTimeChatLog(chat.timestamp.toLong())
            itemView.root.visibility = VISIBLE
            itemView.root_img.visibility = GONE
            itemView.root_voice_record.visibility = GONE

        } else if (chat?.messageType == ConstHelper.IMAGE && chat.imagePath != "") {
            itemView.root.visibility = GONE
            itemView.root_img.visibility = VISIBLE
            itemView.root_voice_record.visibility = GONE

            glide.load(chat.imagePath).fitCenter().into(itemView.imageView_message_image)
            itemView.textView_message_time.text = DateTimeHelper.getFormattedTimeChatLog(chat.timestamp.toLong())
        }
        else if (chat?.messageType == ConstHelper.VOICE && chat.uri.isNotEmpty()) {
            itemView.root.visibility = GONE
            itemView.root_img.visibility = GONE
            itemView.root_voice_record.visibility = VISIBLE
            chat.isPlaying = false
            itemView.progressbar.max = 0
            itemView.durationTextView.text = ""
            itemView.playPauseImage.setOnClickListener {

            }

        }
//        else {
//            itemView.show_message.text = chat?.message
//            itemView.txt_seen.text = DateTimeUtils.getFormattedTimeChatLog(chat?.timestamp?.toLong()
//                    ?: 0)
//        }
    }
}

//        else if (position == sizeListMessages - 1) {
//            if (chat?.isseen == true) {
//                itemView.textView_message_time.text = "Seen"
//            } else {
//                itemView.textView_message_time.text = "Delivered"
//            }
//        }

