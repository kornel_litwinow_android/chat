package kornel.litwinow.chat.viewholders

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestManager
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kornel.litwinow.chat.model.Chat
import kornel.litwinow.chat.model.User
import kornel.litwinow.chat.R
import kornel.litwinow.chat.helpers.DateTimeHelper
import kotlinx.android.synthetic.main.user_item.view.*

class UserV2ViewHolder(val glide: RequestManager, val view: View) : RecyclerView.ViewHolder(view) {

    fun bind(user: User?, listener: UserListener?, isChat: Boolean, isHideLastMessage: Boolean) {
        if (user?.imageURL == "default") {
            itemView.profile_image.setImageResource(R.mipmap.ic_person_placeholder)
        } else {
            glide.load(user?.imageURL).fitCenter().into(itemView.profile_image)
        }
        itemView.username.text = view.context.getString(R.string.profile_name_lastname,user?.username, user?.lastname)

        if (isChat) {
            if (user?.status.equals("online")) {
                itemView.img_on.visibility = View.VISIBLE
                itemView.img_off.visibility = View.GONE
            } else {
                itemView.img_on.visibility = View.GONE
                itemView.img_off.visibility = View.VISIBLE
            }
        } else {
            itemView.img_on.visibility = View.GONE
            itemView.img_off.visibility = View.GONE
        }

        if (isHideLastMessage) {
            itemView.last_msg.visibility = View.GONE
        }

        if (isChat) {
            lastMessage(user?.id)
        } else {
            itemView.last_msg.visibility = View.GONE
        }

        itemView.setOnClickListener { user?.id?.let { it1 -> listener?.goUserMessage(it1) } }

    }

    private fun lastMessage(id: String?) {
        var theLastMessage = ""
        var time = ""
        var isseen = true
        var chat: Chat? = Chat()
        val list = mutableListOf<Chat>()

        val firebaseUser = FirebaseAuth.getInstance().currentUser
        val reference = FirebaseDatabase.getInstance().getReference("Chats")

        reference.addValueEventListener(object : ValueEventListener {

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val children = dataSnapshot.children
                children.forEach {
                    it.key
                    chat = it.getValue(Chat::class.java)
                    if (firebaseUser != null && (chat?.receiver?.equals(firebaseUser.uid)!!) && chat?.sender == id
                            || chat?.receiver.equals(id) && chat?.sender.equals(firebaseUser?.uid)) {
                        theLastMessage = chat?.message.toString()
                        time = chat?.timestamp!!
                        isseen = chat?.isseen ?: true
                        list.add(chat!!)
                    }
                }

                if ("default" == theLastMessage) {
                    itemView.last_msg.text = view.context.getString(R.string.no_message)
                } else {
                    itemView.last_msg.text = theLastMessage
                    itemView.txt_time.text = DateTimeHelper.getFormattedTime(time.toLong())
                }

                if (theLastMessage == "") {
                    itemView.last_msg.text = "Sent a photo \uD83D\uDC40"
                    itemView.txt_time.text = DateTimeHelper.getFormattedTime(time.toLong())
                }

            }

//                if (!isseen) {
//                    val k =view.context.getString(R.string.last_message_bold,theLastMessage)
//                    itemView.last_msg.text = HtmlHelper.fixText(k)
//                }


            override fun onCancelled(p0: DatabaseError) {
            }


        })
    }

}

interface UserListener {
    fun goUserMessage(userId: String)
}