package kornel.litwinow.chat.viewholders

import android.media.MediaPlayer
import android.os.CountDownTimer
import android.view.View
import android.view.View.*
import android.widget.ImageView
import android.widget.ProgressBar
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestManager
import kornel.litwinow.chat.model.Chat
import kornel.litwinow.chat.R
import kornel.litwinow.chat.helpers.ConstHelper
import kornel.litwinow.chat.helpers.DateTimeHelper
import kotlinx.android.synthetic.main.chat_item_left.view.*
import kotlinx.android.synthetic.main.chat_item_left.view.root
import kotlinx.android.synthetic.main.item_image_message.view.*
import kotlinx.android.synthetic.main.item_voice_record_message.view.*
import java.io.IOException

class MessageLeftV2ViewHolder(val glide: RequestManager, val view: View) : RecyclerView.ViewHolder(view) {
    private var player = MediaPlayer()
    private lateinit var countDownTimer: CountDownTimer
    fun bind(chat: Chat?, imageurl: String?, position: Int?, sizeListMessages: Int) {

        if (imageurl != null && imageurl == "default") {
            itemView.profile_image.setImageResource(R.mipmap.ic_person_placeholder)
        } else {
            glide.load(imageurl).fitCenter().into(itemView.profile_image)
        }

        if (chat?.messageType == ConstHelper.TEXT) {
            itemView.show_message.text = chat.message
            itemView.txt_seen.text = DateTimeHelper.getFormattedTimeChatLog(chat.timestamp.toLong())
            itemView.root.visibility = VISIBLE
            itemView.root_img.visibility = GONE
            itemView.root_voice_record.visibility = GONE

        } else if (chat?.messageType == ConstHelper.IMAGE && chat?.imagePath != "") {
            itemView.root.visibility = GONE
            itemView.root_img.visibility = VISIBLE
            itemView.root_voice_record.visibility = GONE

            itemView.textView_message_time.text = DateTimeHelper.getFormattedTimeChatLog(chat?.timestamp?.toLong()
                    ?: 0)
            glide.load(chat.imagePath).fitCenter().into(itemView.imageView_message_image)
        } else if (chat?.messageType == ConstHelper.VOICE && chat.uri.isNotEmpty()) {
            chat.isPlaying = false
            itemView.root.visibility = GONE
            itemView.root_img.visibility = GONE
            itemView.root_voice_record.visibility = VISIBLE
            itemView.progressbar.max = 0
            itemView.durationTextView.text = ""
            itemView.playPauseImage.setOnClickListener {
                startPlaying(
                        chat.uri,
                        chat,
                        itemView.playPauseImage,
                        itemView.progressbar
                )
            }

        }
//        else {
//            itemView.show_message.text = chat?.message
//            itemView.txt_seen.text = DateTimeUtils.getFormattedTimeChatLog(chat?.timestamp?.toLong()
//                    ?: 0)
//        }
    }

    private fun startPlaying(uri: String, chat: Chat, playPauseImage: ImageView?, progressbar: ProgressBar?) {

        if (!chat.isPlaying) {

            stopPlaying()
            chat.isPlaying = false

            player.apply {
                try {
                    setDataSource(uri)
                    prepareAsync()
                } catch (e: IOException) {
                    println("ChatFragment.startPlaying:prepare failed")
                }

                setOnPreparedListener {
                    //media downloaded and will play

                    chat.isPlaying = true
                    //play the record
                    start()

                    //change image to stop and show progress of record
                    progressbar?.max = player.duration
                    playPauseImage?.setImageResource(R.drawable.ic_pause)

                    //count down timer to show record progess but on when record is playing
                    countDownTimer = object : CountDownTimer(player.duration.toLong(), 50) {
                        override fun onFinish() {

                            progressbar?.progress = (player.duration)
                            playPauseImage?.setImageResource(R.drawable.ic_play)

                        }

                        override fun onTick(millisUntilFinished: Long) {

                            progressbar?.progress = (player.duration.minus(millisUntilFinished)).toInt()
                        }

                    }.start()
                }
            }

        } else {
            //stop the record
            playPauseImage?.setImageResource(R.drawable.ic_play)
            stopPlaying()
            chat.isPlaying = false
            progressbar?.progress = 0

        }


    }

    private fun stopPlaying() {
        if (::countDownTimer.isInitialized)
            countDownTimer.cancel()
        player.reset()
    }

}


/*
//        if (position != null && position == sizeListMessages - 1) {
//            if (chat != null && chat.isseen) {
//                itemView.txt_seen.text = view.context.getString(R.string.seen)
//            } else {
//                itemView.txt_seen.text = view.context.getString(R.string.delivered)
//            }
//        } else {
//            itemView.txt_seen.visibility = GONE
//        }
 */