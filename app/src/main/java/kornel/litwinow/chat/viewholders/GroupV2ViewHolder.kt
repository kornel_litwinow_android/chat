package kornel.litwinow.chat.viewholders

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestManager
import kornel.litwinow.chat.model.Group
import kornel.litwinow.chat.R
import kotlinx.android.synthetic.main.group_item.view.*

class GroupV2ViewHolder(
        val glide: RequestManager,
        val view: View) : RecyclerView.ViewHolder(view) {

    fun bind(groupV2: Group?, listener: GroupListener?) {
        itemView.groupName.text = groupV2?.groupName
        if (groupV2?.imageURL == "default") {
            itemView.group_image.setImageResource(R.mipmap.ic_group_place_holder)
        } else {
            glide.load(groupV2?.imageURL).fitCenter().into(itemView.group_image)
        }
        itemView.setOnClickListener { groupV2?.groupName?.let { it1 -> listener?.goMessagesGroup(it1) } }
    }
}


interface GroupListener {
    fun goMessagesGroup(nameGroup: String)
}