package kornel.litwinow.chat.viewholders

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestManager
import kornel.litwinow.chat.R
import kornel.litwinow.chat.model.User
import kotlinx.android.synthetic.main.online_user_item.view.*

class OnlineUserViewHolder(val glide: RequestManager, val view: View) : RecyclerView.ViewHolder(view) {

    fun bind(user: User?, listener: UserListener?) {
        if (user?.imageURL == "default") {
            itemView.profile_image.setImageResource(R.mipmap.ic_person_placeholder)
        } else {
            glide.load(user?.imageURL).fitCenter().into(itemView.profile_image)
        }
        itemView.username.text =  view.context.getString(R.string.profile_name_lastname,user?.username, user?.lastname)

        if (user?.status.equals("online")) {
            itemView.img_on.visibility = View.VISIBLE
            itemView.img_off.visibility = View.GONE
        } else {
            itemView.img_on.visibility = View.GONE
            itemView.img_off.visibility = View.VISIBLE
        }

        itemView.setOnClickListener { user?.id?.let { it1 -> listener?.goUserMessage(it1) } }

    }
}