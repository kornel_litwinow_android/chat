package kornel.litwinow.chat.viewholders

import android.view.View
import android.view.View.GONE
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestManager
import kornel.litwinow.chat.model.GroupMessages
import kornel.litwinow.chat.R
import kornel.litwinow.chat.helpers.ConstHelper
import kornel.litwinow.chat.helpers.DateTimeHelper
import kotlinx.android.synthetic.main.chat_item_left.view.profile_image
import kotlinx.android.synthetic.main.chat_item_left.view.root
import kotlinx.android.synthetic.main.chat_item_left.view.root_img
import kotlinx.android.synthetic.main.chat_item_left.view.show_message
import kotlinx.android.synthetic.main.chat_item_left.view.txt_seen
import kotlinx.android.synthetic.main.item_image_message.view.*

class GroupMessageLeftV2ViewHolder(val glide: RequestManager, val view: View) : RecyclerView.ViewHolder(view) {
    fun bind(groupMessages: GroupMessages?, imageurl: String?, position: Int?, sizeListMessages: Int) {
        if (imageurl != null && imageurl == "default") {
            itemView.profile_image.setImageResource(R.mipmap.ic_person_placeholder)
        } else {
            glide.load(imageurl).fitCenter().into(itemView.profile_image)
        }

        if (groupMessages?.messageType == ConstHelper.TEXT) {
            itemView.show_message.text = groupMessages.message
            itemView.txt_seen.text = DateTimeHelper.getFormattedTimeChatLog(groupMessages.timestamp.toLong())
            itemView.root.visibility = View.VISIBLE
            itemView.root_img.visibility = View.GONE
        } else if (groupMessages?.messageType == ConstHelper.IMAGE && groupMessages.imagePath != "") {
            itemView.root.visibility = GONE
            itemView.root_img.visibility = View.VISIBLE
            itemView.textView_message_time.text = DateTimeHelper.getFormattedTimeChatLog(groupMessages.timestamp.toLong()
                    ?: 0)
            glide.load(groupMessages.imagePath).fitCenter().into(itemView.imageView_message_image)
        } else {
            itemView.show_message.text = groupMessages?.message
            itemView.txt_seen.text = DateTimeHelper.getFormattedTimeChatLog(groupMessages?.timestamp?.toLong()
                    ?: 0)
        }

    }
}


// if (position != null && position == sizeListMessages - 1) {
//            if (groupMessages != null && groupMessages.isseen) {
//                itemView.txt_seen.text = view.context.getString(R.string.seen)
//            } else {
//                itemView.txt_seen.text = view.context.getString(R.string.delivered)
//            }
//        } else {
//            itemView.txt_seen.visibility = GONE
//        }