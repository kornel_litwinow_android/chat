package kornel.litwinow.chat.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.iid.FirebaseInstanceId
import kornel.litwinow.chat.helpers.ConstHelper
import kornel.litwinow.chat.helpers.AuthHelper
import kornel.litwinow.chat.model.*
import kornel.litwinow.chat.notifications.TokenV2
import kornel.litwinow.chat.viewmodels.MainActivityViewModel.MainActivityViewState.*

class MainActivityViewModel : BaseViewModel() {

    private val persons = mutableListOf<Person>()
    private var groupList2 = mutableListOf<Group>()

    private val mutableMainActivityViewState = MutableLiveData<MainActivityViewState>()
    val liveDataMainActivityViewState: LiveData<MainActivityViewState> = mutableMainActivityViewState

    private var userList = mutableListOf<User>()
    private var groupList = mutableListOf<Group>()
    private var chatListList = mutableListOf<ChatList>()
    private var userChatList = mutableListOf<User>()


    init {
        getSelfInfo()
        initPagerInfo()
        readPersons()
    }

    private fun getSelfInfo() {
        mutableMainActivityViewState.value = Loading
        val reference = FirebaseDatabase.getInstance().getReference(ConstHelper.USERS)
                .child(AuthHelper.auth?.uid ?: "")
        reference.addValueEventListener(object : ValueEventListener {

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val user = dataSnapshot.getValue(User::class.java)
                mutableMainActivityViewState.value = UserInfo(user)
            }

            override fun onCancelled(p0: DatabaseError) {
                mutableMainActivityViewState.value = Error
            }

        })
    }

    private fun initPagerInfo() {
        val reference = FirebaseDatabase.getInstance().getReference(ConstHelper.CHATS)
        reference.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                var unread = 0
                dataSnapshot.children.forEach {
                    val chat = it.getValue(Chat::class.java)
                    if (chat?.receiver.equals(AuthHelper.auth?.uid) && chat?.isseen != true) {
                        unread++
                    }
                    mutableMainActivityViewState.value = PagerInfo(unread)
                }
            }

            override fun onCancelled(p0: DatabaseError) {
                mutableMainActivityViewState.value = Error
            }

        })
    }

    fun getOnlineUsers() {
        val reference = FirebaseDatabase.getInstance().getReference(ConstHelper.USERS)
        reference.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                userList.clear()
                dataSnapshot.children.forEach {
                    val user = it.getValue(User::class.java)
                    if (user?.id != AuthHelper.auth?.currentUser?.uid && user?.status == ConstHelper.ONLINE) {
                        userList.add(user)
                    }
                }
                if (userList.isNullOrEmpty()) {
                    mutableMainActivityViewState.value = EmptyList

                } else {
                    mutableMainActivityViewState.value = OnlineUsers(userList)
                }
            }

            override fun onCancelled(p0: DatabaseError) {
            }

        })
    }

    fun createGroup(groupName: String?) {
        if (!groupName.isNullOrEmpty()) {
            val idFounder = AuthHelper.auth?.uid
            val reference = FirebaseDatabase.getInstance().getReference(ConstHelper.GROUPS).child(groupName)
            val hashMap = HashMap<String, String>()
            hashMap[ConstHelper.ID_FOUNDER] = idFounder.toString()
            hashMap[ConstHelper.GROUP_NAME] = groupName
            hashMap[ConstHelper.IMAGE_URL] = "default"

            reference.setValue(hashMap).addOnCompleteListener {
                if (it.isSuccessful) {
                    mutableMainActivityViewState.value = AddGroupSuccess
                } else {
                    mutableMainActivityViewState.value = AddGroupFailed
                }
            }
        }
    }

    fun getGroups() {
        val reference = FirebaseDatabase.getInstance().reference.child(ConstHelper.GROUPS)
        reference.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                groupList.clear()
                groupList2.clear()
                dataSnapshot.children.forEach {
                    val group = it.getValue(Group::class.java)
                    if (group != null) {
                        groupList.add(group)
                    }
                }
                if (groupList.isNullOrEmpty()) {
                    mutableMainActivityViewState.value = EmptyList
                } else {
                    val nameGroupList = mutableListOf<String>()
                    val nameGroupListPerson = mutableListOf<String>()
                    groupList.forEach { nameGroupList.add(it.groupName) }
                    persons.forEach {
                        if (it.id == AuthHelper.auth?.currentUser?.uid) {
                            nameGroupListPerson.add(it.nameGroup)
                        }
                    }

                    groupList.forEach {
                        if (nameGroupListPerson.contains(it.groupName) || it.idFounder == AuthHelper.auth?.currentUser?.uid) {
                            groupList2.add(it)
                        }
                    }

                    mutableMainActivityViewState.value = Groups(groupList2)
                }
            }

            override fun onCancelled(p0: DatabaseError) {
            }

        })
    }

    fun updateToken() {
        val reference = FirebaseDatabase.getInstance().getReference(ConstHelper.TOKENS)
        val token = TokenV2(FirebaseInstanceId.getInstance().token ?: "")
        reference.child(AuthHelper.auth?.uid ?: "").setValue(token)
    }

    fun getUserChatList() {
        val reference = FirebaseDatabase.getInstance().getReference(ConstHelper.USERS)
        reference.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                userChatList.clear()
                dataSnapshot.children.forEach {
                    val user = it.getValue(User::class.java)
                    chatListList.forEach { c ->
                        if (user?.id.equals(c.id)) {
                            userChatList.add(user!!)
                        }
                    }
                }
                if (userChatList.isNullOrEmpty()) {
                    mutableMainActivityViewState.value = EmptyList
                } else {
                    mutableMainActivityViewState.value = InitUserList(userChatList)
                }
            }

            override fun onCancelled(p0: DatabaseError) {
            }

        })
    }

    fun initChatList() {
        val reference = FirebaseDatabase.getInstance().getReference(ConstHelper.CHAT_LIST)
                .child(AuthHelper.auth?.currentUser?.uid
                        ?: "")
        reference.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                chatListList.clear()
                dataSnapshot.children.forEach {
                    val chatList = it.getValue(ChatList::class.java)
                    if (chatList != null) {
                        chatListList.add(chatList)
                    }
                }
                getUserChatList()
            }

            override fun onCancelled(p0: DatabaseError) {
            }

        })
    }

    private fun readPersons() {
        val reference = FirebaseDatabase.getInstance().getReference(("Person"))
        reference.addValueEventListener(object : ValueEventListener {

            override fun onDataChange(dataSnapShot: DataSnapshot) {
                persons.clear()
                dataSnapShot.children.forEach {
                    val person = it.getValue(Person::class.java)
                    if (person != null) {
                        persons.add(person)
                    }
                }

            }

            override fun onCancelled(p0: DatabaseError) {
            }

        })
    }


    sealed class MainActivityViewState {
        object Loading : MainActivityViewState()
        object Error : MainActivityViewState()
        data class UserInfo(val user: User?) : MainActivityViewState()
        data class PagerInfo(val unread: Int) : MainActivityViewState()
        data class OnlineUsers(val userList: List<User>) : MainActivityViewState()
        object EmptyList : MainActivityViewState()
        data class Groups(val groupList: List<Group>) : MainActivityViewState()
        object AddGroupSuccess : MainActivityViewState()
        object AddGroupFailed : MainActivityViewState()
        data class InitUserList(val chatList: List<User>) : MainActivityViewState()
    }
}
