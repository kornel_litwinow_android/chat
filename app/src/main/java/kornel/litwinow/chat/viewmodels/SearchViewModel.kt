package kornel.litwinow.chat.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kornel.litwinow.chat.helpers.ConstHelper
import kornel.litwinow.chat.helpers.AuthHelper
import kornel.litwinow.chat.model.User
import kornel.litwinow.chat.viewmodels.SearchViewModel.SearchViewState.*

class SearchViewModel : BaseViewModel() {

    private var userList = mutableListOf<User>()
    private val mutableSearchViewState = MutableLiveData<SearchViewState>()
    val liveDataSearchViewState : LiveData<SearchViewState> = mutableSearchViewState

    fun getUsers(text: String) {
        if (text.isEmpty()) {
            mutableSearchViewState.value = Loading
            val firebaseUser = AuthHelper.auth?.currentUser
            val reference = FirebaseDatabase.getInstance().getReference(ConstHelper.USERS)
            reference.addValueEventListener(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    userList.clear()
                    dataSnapshot.children.forEach {
                        val user = it.getValue(User::class.java)
                        if (!user?.id.equals(firebaseUser?.uid) && user != null) {
                            userList.add(user)
                        }
                    }
                    mutableSearchViewState.value = Success(userList)
                }

                override fun onCancelled(p0: DatabaseError) {
                    mutableSearchViewState.value = Error
                }

            })
        } else {
            searchUsers(text)
        }
    }

     fun searchUsers(text: String) {
         mutableSearchViewState.value = Loading
        val firebaseUser = AuthHelper.auth?.currentUser
         val query = FirebaseDatabase.getInstance().getReference(ConstHelper.USERS)
                 .orderByChild(ConstHelper.SEARCH).startAt(text).endAt(text +"\uf8ff")
         query.addValueEventListener(object : ValueEventListener {
             override fun onDataChange(dataSnapshot: DataSnapshot) {
                 userList.clear()
                 dataSnapshot.children.forEach {
                     val user = it.getValue(User::class.java)
                     if (!user?.id.equals(firebaseUser?.uid) && user != null) {
                         userList.add(user)
                     }
                 }
                 mutableSearchViewState.value = Success(userList)
             }
             override fun onCancelled(p0: DatabaseError) {
                 mutableSearchViewState.value = Error
             }

         })
    }

    sealed class SearchViewState {
        object Loading : SearchViewState()
        data class Success(val userList: List<User>) : SearchViewState()
        object Error : SearchViewState()
    }
}