package kornel.litwinow.chat.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kornel.litwinow.chat.helpers.ConstHelper
import kornel.litwinow.chat.helpers.AuthHelper
import kornel.litwinow.chat.model.Person
import kornel.litwinow.chat.model.User
import kornel.litwinow.chat.viewmodels.AddGroupMemberViewModel.AddGroupMemberViewState.*

class AddGroupMemberViewModel : ViewModel() {

    private var userList = mutableListOf<User>()
    private val mutableMemberViewState = MutableLiveData<AddGroupMemberViewState>()
    val liveDataMemberViewState: LiveData<AddGroupMemberViewState> = mutableMemberViewState
    private var personList = mutableListOf<Person>()
    private var userList2 = mutableListOf<User>()


    fun getUsers(text: String, name: String) {
        getGroupMembers(name)
        if (text.isEmpty()) {
            mutableMemberViewState.value = Loading
            val firebaseUser = AuthHelper.auth?.currentUser
            val reference = FirebaseDatabase.getInstance().getReference(ConstHelper.USERS)
            reference.addValueEventListener(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    userList.clear()
                    userList2.clear()
                    dataSnapshot.children.forEach {
                        val user = it.getValue(User::class.java)
                        if (!user?.id.equals(firebaseUser?.uid) && user != null) {
                            userList.add(user)
                        }
                    }
                    val k = userList.map { it.id }
                    val l = personList.map { it.id }

                    val m = k - l
                    val listWithoutRepeat = m.distinct()
                    userList.forEach {
                        if (it.id != AuthHelper.auth?.currentUser?.uid && listWithoutRepeat.contains(it.id)) {
                            userList2.add(it)
                        }
                    }

                    mutableMemberViewState.value = Success(userList2)
                }

                override fun onCancelled(p0: DatabaseError) {
                    mutableMemberViewState.value = Error
                }

            })
        } else {
            searchUsers(text)
        }
    }

    fun searchUsers(text: String) {
        mutableMemberViewState.value = Loading
        val firebaseUser = AuthHelper.auth?.currentUser
        val query = FirebaseDatabase.getInstance().getReference(ConstHelper.USERS)
                .orderByChild(ConstHelper.SEARCH).startAt(text).endAt(text + "\uf8ff")
        query.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                userList.clear()
                userList2.clear()
                dataSnapshot.children.forEach {
                    val user = it.getValue(User::class.java)
                    if (!user?.id.equals(firebaseUser?.uid) && user != null) {
                        userList.add(user)
                    }
                }
                val k = userList.map { it.id }
                val l = personList.map { it.id }

                val m = k - l
                val listWithOutRepeats = m.distinct()
                userList.forEach {
                    if (it.id != AuthHelper.auth?.currentUser?.uid && listWithOutRepeats.contains(it.id)) {
                        userList2.add(it)
                    }
                }

                mutableMemberViewState.value = Success(userList2)
            }

            override fun onCancelled(p0: DatabaseError) {
                mutableMemberViewState.value = Error
            }

        })
    }

    private fun getGroupMembers(nameGroup: String) {
        val reference = FirebaseDatabase.getInstance().getReference(ConstHelper.GROUPS).child(nameGroup).child("Person")
        reference.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                personList.clear()
                dataSnapshot.children.forEach {
                    val person = it.getValue(Person::class.java)
                    if (person != null) {
                        personList.add(person)
                    }
                }

            }

            override fun onCancelled(p0: DatabaseError) {
            }

        })
    }

    fun addMemberToPerson(userId: String, name: String) {
        val reference = FirebaseDatabase.getInstance().getReference("Person")

        val hashMap = HashMap<String, String>()
        hashMap["id"] = userId
        hashMap["admin"] = "false"
        hashMap["nameGroup"] = name
        reference.push().setValue(hashMap)
        addMemberToGroup(userId, name)
    }

    private fun addMemberToGroup(userId: String, name: String) {
        val reference = FirebaseDatabase.getInstance().getReference("Groups").child(name).child("Person")
        val keyreference = reference.child(userId)

        val hashMap = HashMap<String, String>()
        hashMap["id"] = userId
        hashMap["admin"] = "false"
        keyreference.setValue(hashMap)

    }

    sealed class AddGroupMemberViewState {
        object Loading : AddGroupMemberViewState()
        data class Success(val userList: List<User>) : AddGroupMemberViewState()
        object Error : AddGroupMemberViewState()
    }
}