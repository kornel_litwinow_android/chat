package kornel.litwinow.chat.viewmodels

import android.content.ContentResolver
import android.net.Uri
import android.webkit.MimeTypeMap
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.StorageTask
import com.google.firebase.storage.UploadTask
import kornel.litwinow.chat.helpers.ConstHelper
import kornel.litwinow.chat.helpers.AuthHelper
import kornel.litwinow.chat.model.User
import kornel.litwinow.chat.viewmodels.ProfileViewModel.ProfileViewState.*

class ProfileViewModel : BaseViewModel() {
    private var reference: DatabaseReference? = null

    private var storageReference: StorageReference? = null
    var uploadTask: StorageTask<UploadTask.TaskSnapshot>? = null

    private val mutableProfileViewState = MutableLiveData<ProfileViewState>()
    val liveDataProfileViewState: LiveData<ProfileViewState> = mutableProfileViewState


    fun getUserInfo() {
        mutableProfileViewState.value = Loading
        reference = FirebaseDatabase.getInstance().getReference(ConstHelper.USERS).child(AuthHelper.auth?.currentUser?.uid
                ?: "")
        reference?.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val response = dataSnapshot.getValue(User::class.java)
                mutableProfileViewState.value = Success(response)
            }

            override fun onCancelled(p0: DatabaseError) {
                mutableProfileViewState.value = Error
            }

        })
    }

    fun uploadImage(imageUri: Uri?, contentResolver: ContentResolver) {
        storageReference = FirebaseStorage.getInstance().getReference(ConstHelper.UPLOADS)
        mutableProfileViewState.value = ProgressUpdateImage
        if (imageUri != null) {
            val fileReference: StorageReference? = storageReference?.child(System.currentTimeMillis().toString() + "." + getFileExtension(imageUri, contentResolver))
            uploadTask = fileReference?.putFile(imageUri)
            uploadTask?.continueWithTask {
                return@continueWithTask fileReference?.downloadUrl
            }?.addOnCompleteListener {
                if (it.isSuccessful) {
                    val downloadUri = it.result
                    val mUri = downloadUri.toString()
                    reference = FirebaseDatabase.getInstance().getReference(ConstHelper.USERS).child(AuthHelper.auth?.currentUser?.uid
                            ?: "")
                    val hashMap = HashMap<String, Any>()
                    hashMap[ConstHelper.IMAGE_URL] = mUri
                    reference?.updateChildren(hashMap)
                    mutableProfileViewState.value = SuccessUpdateImage
                } else {
                    mutableProfileViewState.value = FailedUpdateImage
                }
            }?.addOnFailureListener {
                mutableProfileViewState.value = FailedUpdateImage
            }
        }
    }

    private fun getFileExtension(uri: Uri, contentResolver: ContentResolver): String? {
        val mimeTypeMap = MimeTypeMap.getSingleton()
        return mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(uri))
    }

    sealed class ProfileViewState {
        object Loading : ProfileViewState()
        object Error : ProfileViewState()
        data class Success(val user: User?) : ProfileViewState()
        object SuccessUpdateImage : ProfileViewState()
        object ProgressUpdateImage : ProfileViewState()
        object FailedUpdateImage : ProfileViewState()
    }
}