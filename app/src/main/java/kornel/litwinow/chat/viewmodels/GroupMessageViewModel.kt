package kornel.litwinow.chat.viewmodels

import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import kornel.litwinow.chat.helpers.ConstHelper
import kornel.litwinow.chat.fragments.ApiV2Service
import kornel.litwinow.chat.helpers.AuthHelper
import kornel.litwinow.chat.model.GroupMessages
import kornel.litwinow.chat.model.User

class GroupMessageViewModel : BaseViewModel() {

    private var currentLastName: String = ""
    var currentUserName: String = ""
    private var seenReference: DatabaseReference? = null
    private var seenListener: ValueEventListener? = null

    private val mutableMessageViewState = MutableLiveData<GroupMessageViewState>()
    val messageViewState: LiveData<GroupMessageViewState> = mutableMessageViewState

    private var apiV2Service: ApiV2Service? = null
    private var notify = false
    var nameGroup: String = ""
    var messages = mutableListOf<GroupMessages>()


    fun getUsersToTalk() {
        val reference = FirebaseDatabase.getInstance().reference.child(ConstHelper.USERS)
        reference.child(AuthHelper.auth?.currentUser?.uid.toString()).addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapShot: DataSnapshot) {
                if (dataSnapShot.exists()) {
                    currentUserName = dataSnapShot.child(ConstHelper.USERNAME).value.toString()
                    currentLastName = dataSnapShot.child(ConstHelper.LASTNAME).value.toString()
                }
                readMessage()
            }

            override fun onCancelled(p0: DatabaseError) {
            }

        })
    }

    private fun readMessage() {
        val reference = FirebaseDatabase.getInstance().getReference(ConstHelper.GROUPS).child(nameGroup).child(ConstHelper.GROUP_MESSAGES)
        reference.addValueEventListener(object : ValueEventListener {

            override fun onDataChange(dataSnapShot: DataSnapshot) {
                messages.clear()
                dataSnapShot.children.forEach {
                    val groupMessages = it.getValue(GroupMessages::class.java)
                    if (groupMessages != null) {
                        messages.add(groupMessages)
                    }
                }

                mutableMessageViewState.value = GroupMessageViewState.Messages(messages)
            }

            override fun onCancelled(p0: DatabaseError) {
            }

        })
    }

    fun sendMessage(message: String, imagePath: String, messageType: String = ConstHelper.TEXT) {
        notify = true
        val timeStamp = System.currentTimeMillis() / 1000

        val groupReference = FirebaseDatabase.getInstance().getReference(ConstHelper.GROUPS).child(nameGroup)
        val messageKey = groupReference.push().key
        val groupMessageKey = HashMap<String, Any>()
        val groupMessages = HashMap<String, Any>()
        groupReference.updateChildren(groupMessages)
        groupReference.updateChildren(groupMessageKey)

        val groupChatReference = groupReference.child("GroupMessages")
        val groupReferenceKey = groupChatReference.child(messageKey.toString())

        val hashMap = HashMap<String, Any>()

        hashMap[ConstHelper.SENDER] = AuthHelper.auth?.currentUser?.uid.toString()
        hashMap[ConstHelper.NAME] = currentUserName
        hashMap[ConstHelper.LASTNAME] = currentLastName
        hashMap[ConstHelper.MESSAGE] = message
        hashMap[ConstHelper.IS_SEEN] = false
        hashMap[ConstHelper.TIMESTAMP] = timeStamp.toString()
        hashMap[ConstHelper.MESSAGE_TYPE] = messageType
        hashMap[ConstHelper.IMAGE_PATH] = imagePath

        groupReferenceKey.updateChildren(hashMap)
    }

    fun uploadImage(uri: Uri?, extension: String) {
        val storageReference = FirebaseStorage.getInstance().getReference(ConstHelper.UPLOADS)
        mutableMessageViewState.value = GroupMessageViewState.LoadingImage
        if (uri != null) {
            val fileReference: StorageReference? = storageReference.child(System.currentTimeMillis().toString() + "." + extension)
            val uploadTask = fileReference?.putFile(uri)
            uploadTask?.continueWithTask {
                return@continueWithTask fileReference?.downloadUrl
            }?.addOnCompleteListener {
                if (it.isSuccessful) {
                    val downloadUri = it.result
                    val mUri = downloadUri.toString()
                    sendMessage(message = "", imagePath = mUri, messageType = ConstHelper.IMAGE)

                    mutableMessageViewState.value = GroupMessageViewState.HideLoadingImage
                } else {
                    mutableMessageViewState.value = GroupMessageViewState.HideLoadingImage
                }
            }?.addOnFailureListener {
                mutableMessageViewState.value = GroupMessageViewState.FailUpdateImage
            }
        }
    }

    fun seenMessage() {
        seenReference = FirebaseDatabase.getInstance().getReference(ConstHelper.GROUPS).child(nameGroup).child(ConstHelper.GROUP_MESSAGES)
        seenListener = seenReference?.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                for (snapshot in dataSnapshot.children) {
                    val groupMessage = snapshot.getValue(GroupMessages::class.java)!!
                    if (groupMessage.sender != AuthHelper.auth?.currentUser?.uid) {
                        val hashMap = HashMap<String, Any>()
                        hashMap[ConstHelper.IS_SEEN] = true
                        snapshot.ref.updateChildren(hashMap)
                    }
                }
            }

            override fun onCancelled(p0: DatabaseError) {
            }

        })
    }

    override fun onCleared() {
        super.onCleared()
        seenListener.let {
            if (it != null) {
                seenReference?.removeEventListener(it)
            }
        }
    }

    sealed class GroupMessageViewState {
        data class Messages(val listMessages: List<GroupMessages>) : GroupMessageViewState()
        object LoadingImage : GroupMessageViewState()
        object HideLoadingImage : GroupMessageViewState()
        object FailUpdateImage : GroupMessageViewState()
    }
}