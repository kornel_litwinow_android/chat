package kornel.litwinow.chat.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kornel.litwinow.chat.helpers.AuthHelper
import kornel.litwinow.chat.viewmodels.ResetPasswordViewModel.ResetPasswordViewState.*

class ResetPasswordViewModel : BaseViewModel() {
    private val mutableViewState = MutableLiveData<ResetPasswordViewState>()
    val liveDataViewState: LiveData<ResetPasswordViewState> = mutableViewState

    fun resetPassword(email: String) {
        mutableViewState.value = Loading
        AuthHelper.auth?.sendPasswordResetEmail(email)?.addOnCompleteListener {
            if (it.isSuccessful) {
                mutableViewState.value = Success
            } else {
                mutableViewState.value = Error(it.exception?.message)
            }
        }
    }

    sealed class ResetPasswordViewState {
        object Loading : ResetPasswordViewState()
        object Success : ResetPasswordViewState()
        data class Error(val error: String?) : ResetPasswordViewState()
    }
}