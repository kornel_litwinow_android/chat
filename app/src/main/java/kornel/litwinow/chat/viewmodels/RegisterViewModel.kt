package kornel.litwinow.chat.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kornel.litwinow.chat.helpers.ConstHelper
import kornel.litwinow.chat.helpers.AuthHelper
import kornel.litwinow.chat.viewmodels.RegisterViewModel.RegisterViewState.*
import java.util.*
import kotlin.collections.HashMap

class RegisterViewModel : BaseViewModel() {
    private var reference: DatabaseReference? = null
    private val mutableRegisterViewState = MutableLiveData<RegisterViewState>()
    val liveDataRegisterViewState: LiveData<RegisterViewState> = mutableRegisterViewState

    fun register(userName: String?, lastName: String?, email: String?, password: String?) {
        mutableRegisterViewState.value = Loading
        if (email != null && password != null) {
            AuthHelper.auth?.createUserWithEmailAndPassword(email, password)?.addOnCompleteListener {
                if (it.isSuccessful) {
                    val firebaseUser = AuthHelper.auth?.currentUser
                    val userId = firebaseUser?.uid
                    reference = FirebaseDatabase.getInstance().getReference(ConstHelper.USERS).child(userId.toString())
                    val hashMap = HashMap<String, String>()
                    hashMap[ConstHelper.ID] = userId.toString()
                    hashMap[ConstHelper.USERNAME] = userName ?: ""
                    hashMap[ConstHelper.LASTNAME] = lastName ?: ""
                    hashMap[ConstHelper.EMAIL] = email
                    hashMap[ConstHelper.IMAGE_URL] = "default"
                    hashMap[ConstHelper.STATUS] = "offline"
                    hashMap[ConstHelper.SEARCH] = userName?.toLowerCase(Locale.ROOT) ?: ""

                    reference?.setValue(hashMap)?.addOnCompleteListener { result ->
                        if (result.isSuccessful) {
                            mutableRegisterViewState.value = Success
                        }
                    }
                } else {
                    mutableRegisterViewState.value = Error
                }
            }
        }
    }

    sealed class RegisterViewState {
        object Loading : RegisterViewState()
        object Success : RegisterViewState()
        object Error : RegisterViewState()
    }
}