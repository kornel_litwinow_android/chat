package kornel.litwinow.chat.viewmodels

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.github.pwittchen.reactivenetwork.library.rx2.ReactiveNetwork
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit


open class BaseViewModel @ViewModelInject constructor(): ViewModel() {

    private var internetDisposable: Disposable? = null
    var internetStatus = MutableLiveData<Boolean>()

    fun checkInternetConnection() {
        internetDisposable = ReactiveNetwork.observeInternetConnectivity()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).delay(1, TimeUnit.SECONDS)
                .subscribe { isConnectedToInternet ->
                    internetStatus.postValue(isConnectedToInternet)
                }
    }

    override fun onCleared() {
        super.onCleared()
        safelyDispose(internetDisposable)
    }

    private fun safelyDispose(disposable: Disposable?) {
        if (disposable != null && !disposable.isDisposed) {
            disposable.dispose()
        }
    }
}