package kornel.litwinow.chat.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kornel.litwinow.chat.helpers.AuthHelper
import kornel.litwinow.chat.viewmodels.LoginViewModel.LoginViewState.*

class LoginViewModel : BaseViewModel() {
    private val mutableLoginViewState = MutableLiveData<LoginViewState>()
    val liveDataLoginViewState: LiveData<LoginViewState> = mutableLoginViewState

    fun login(email: String, password: String) {
        mutableLoginViewState.value = Loading
        AuthHelper.auth?.signInWithEmailAndPassword(email, password)?.addOnCompleteListener {
            if (it.isSuccessful) {
                mutableLoginViewState.value = Success
            } else {
                mutableLoginViewState.value = Error
            }
        }
    }


    sealed class LoginViewState {
        object Success : LoginViewState()
        object Error : LoginViewState()
        object Loading : LoginViewState()
    }
}