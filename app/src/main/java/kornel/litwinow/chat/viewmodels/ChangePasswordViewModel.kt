package kornel.litwinow.chat.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.auth.EmailAuthProvider
import kornel.litwinow.chat.helpers.AuthHelper
import kornel.litwinow.chat.viewmodels.ChangePasswordViewModel.ChangePasswordViewState.*

class ChangePasswordViewModel : BaseViewModel() {

    private val mutableChangePasswordViewState = MutableLiveData<ChangePasswordViewState>()
    val liveDataLoginViewState: LiveData<ChangePasswordViewState> = mutableChangePasswordViewState

    fun changePassword(oldPassword: String, newPassword: String) {
        mutableChangePasswordViewState.value = Loading
        val user = AuthHelper.auth?.currentUser
        val email = user?.email
        if (email != null) {
            val credential = EmailAuthProvider.getCredential(email, oldPassword)

            user.reauthenticate(credential)?.addOnCompleteListener { result ->
                if (result.isSuccessful) {
                    user.updatePassword(newPassword).addOnCompleteListener {
                        if (it.isSuccessful) {
                            mutableChangePasswordViewState.value = Success
                        } else {
                            mutableChangePasswordViewState.value = Error
                        }
                    }
                }
            }
        }
    }

    sealed class ChangePasswordViewState {
        object Success : ChangePasswordViewState()
        object Error : ChangePasswordViewState()
        object Loading : ChangePasswordViewState()
    }
}