package kornel.litwinow.chat.viewmodels

import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import kornel.litwinow.chat.R
import kornel.litwinow.chat.helpers.ConstHelper
import kornel.litwinow.chat.fragments.ApiV2Service
import kornel.litwinow.chat.helpers.AuthHelper
import kornel.litwinow.chat.model.Chat
import kornel.litwinow.chat.model.User
import kornel.litwinow.chat.notifications.*
import kornel.litwinow.chat.viewmodels.MessageViewModel.MessageViewState.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber

class MessageViewModel : BaseViewModel() {

    private var seenReference: DatabaseReference? = null
    private var seenListener: ValueEventListener? = null
    private val mutableMessageViewState = MutableLiveData<MessageViewState>()
    val messageViewState: LiveData<MessageViewState> = mutableMessageViewState

    private var apiV2Service: ApiV2Service? = null
    private var notify = false
    var userId: String = ""
    var messages = mutableListOf<Chat>()

    fun sendMessage(sender: String, receiver: String, message: String, imagePath: String, messageType: String = ConstHelper.TEXT) {
        notify = true
        var reference = FirebaseDatabase.getInstance().reference
        val timeStamp = System.currentTimeMillis() / 1000

        val hashMap = HashMap<String, Any>()
        hashMap[ConstHelper.SENDER] = sender
        hashMap[ConstHelper.RECEIVER] = receiver
        hashMap[ConstHelper.MESSAGE] = message
        hashMap[ConstHelper.IS_SEEN] = false
        hashMap[ConstHelper.TIMESTAMP] = timeStamp.toString()
        hashMap[ConstHelper.MESSAGE_TYPE] = messageType
        hashMap[ConstHelper.IMAGE_PATH] = imagePath

        reference.child(ConstHelper.CHATS).push().setValue(hashMap)
        val chatRef = FirebaseDatabase.getInstance().getReference(ConstHelper.CHAT_LIST)
                .child(AuthHelper.auth?.uid.toString()).child(userId)
        chatRef.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (!dataSnapshot.exists()) {
                    chatRef.child(ConstHelper.ID).setValue(userId)
                    updateReceiverChatList()
                }
            }

            override fun onCancelled(p0: DatabaseError) {
            }
        })
        reference = FirebaseDatabase.getInstance().getReference(ConstHelper.USERS).child(AuthHelper.auth?.uid
                ?: "")
        reference.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapShot: DataSnapshot) {
                val user = dataSnapShot.getValue(User::class.java)
                if (notify) {
                    if (user != null) {
                        val userInfo = "${user.username} ${user.lastname}"
                        sendNotification(receiver, userInfo, message)
                    }
                }
                notify = false
            }

            override fun onCancelled(p0: DatabaseError) {
            }

        })
    }

    private fun sendNotification(receiver: String, username: String, message: String) {
        apiV2Service = ClientV2.getClient("https://fcm.googleapis.com/")?.create(ApiV2Service::class.java)
        val tokens = FirebaseDatabase.getInstance().getReference(ConstHelper.TOKENS)
        val query = tokens.orderByKey().equalTo(receiver)
        query.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                dataSnapshot.children.forEach {
                    val token = it.getValue(TokenV2::class.java)
                    val data = DataV2(AuthHelper.auth?.uid
                            ?: "", R.mipmap.ic_launcher, "$username:$message", "new message", userId)
                    val sender = SenderV2(data, token?.token!!)

                    apiV2Service?.sendNotification(sender)
                            ?.enqueue(object : Callback<MyResponseV2> {
                                override fun onResponse(call: Call<MyResponseV2>, response: Response<MyResponseV2>) {
                                    Timber.d("ResponseNotification: ${response.isSuccessful}")
                                }

                                override fun onFailure(call: Call<MyResponseV2>, t: Throwable) {
                                }


                            })
                }
            }

            override fun onCancelled(p0: DatabaseError) {
            }

        })
    }

    private fun updateReceiverChatList() {
        val chatRef = FirebaseDatabase.getInstance().getReference(ConstHelper.CHAT_LIST)
                .child(userId).child(AuthHelper.auth?.uid ?: "")

        chatRef.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                chatRef.child(ConstHelper.ID).setValue(AuthHelper.auth?.uid ?: "")
            }

            override fun onCancelled(p0: DatabaseError) {
            }
        })
    }


    fun getUserToTalk() {
        val reference = FirebaseDatabase.getInstance().getReference(ConstHelper.USERS).child(userId)
        reference.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val user = dataSnapshot.getValue(User::class.java)
                mutableMessageViewState.value = UserInfo(user)
                readMessages(AuthHelper.auth?.currentUser?.uid ?: "", userId, user?.imageURL)

            }

            override fun onCancelled(p0: DatabaseError) {
            }
        })
    }

    fun readMessages(myId: String, userId: String, imageURL: String?) {
        mutableMessageViewState.value = LoadMessages
        val reference = FirebaseDatabase.getInstance().getReference(ConstHelper.CHATS)
        reference.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                messages.clear()
                dataSnapshot.children.forEach {
                    val chat = it.getValue(Chat::class.java)
                    if (chat?.receiver == myId && chat.sender == userId
                            || chat?.receiver == userId && chat.sender == myId) {
                        messages.add(chat)
                    }
                }
                mutableMessageViewState.value = Messages(messages, imageURL)

            }

            override fun onCancelled(p0: DatabaseError) {
            }

        })
    }

    override fun onCleared() {
        super.onCleared()
        seenListener.let {
            if (it != null) {
                seenReference?.removeEventListener(it)
            }
        }
    }

    fun seenMessage() {
        seenReference = FirebaseDatabase.getInstance().getReference(ConstHelper.CHATS)
        seenListener = seenReference?.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                for (snapshot in dataSnapshot.children) {
                    val chat = snapshot.getValue(Chat::class.java)!!
                    if (chat.receiver == AuthHelper.auth?.currentUser?.uid && chat.sender == userId) {
                        val hashMap = HashMap<String, Any>()
                        hashMap[ConstHelper.IS_SEEN] = true
                        snapshot.ref.updateChildren(hashMap)
                    }
                }
            }

            override fun onCancelled(p0: DatabaseError) {
            }

        })
    }

    fun uploadImage(uri: Uri?, extension: String) {
        val storageReference = FirebaseStorage.getInstance().getReference(ConstHelper.UPLOADS)
        mutableMessageViewState.value = LoadingImage
        if (uri != null) {
            val fileReference: StorageReference? = storageReference.child(System.currentTimeMillis().toString() + "." + extension)
            val uploadTask = fileReference?.putFile(uri)
            uploadTask?.continueWithTask {
                return@continueWithTask fileReference?.downloadUrl
            }?.addOnCompleteListener {
                if (it.isSuccessful) {
                    val downloadUri = it.result
                    val mUri = downloadUri.toString()
                    sendMessage(AuthHelper.auth?.uid.toString(), userId, message = "", imagePath = mUri, messageType = ConstHelper.IMAGE)

                    mutableMessageViewState.value = HideLoadingImage
                } else {
                    mutableMessageViewState.value = HideLoadingImage
                }
            }?.addOnFailureListener {
                mutableMessageViewState.value = FailUpdateImage
            }
        }
    }


    sealed class MessageViewState {
        object LoadMessages : MessageViewState()
        data class UserInfo(val user: User?) : MessageViewState()
        data class Messages(val listMessages: List<Chat>, val imageURL: String?) : MessageViewState()
        object LoadingImage : MessageViewState()
        object HideLoadingImage : MessageViewState()
        object FailUpdateImage : MessageViewState()
    }

}