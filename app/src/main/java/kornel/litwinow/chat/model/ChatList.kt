package kornel.litwinow.chat.model

import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
data class ChatList(val id: String = "")