package kornel.litwinow.chat.model

import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
data class Group(val idFounder: String = "", val groupName: String = "",
                 val imageURL: String = "",
                 val person: Person = Person())