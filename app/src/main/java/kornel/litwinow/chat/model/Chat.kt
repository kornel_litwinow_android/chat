package kornel.litwinow.chat.model

import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
data class Chat(val sender: String = "", val receiver: String = "",
                val message: String = "", val isseen: Boolean = false, val timestamp: String = "",
                val messageType: String = "",val imagePath: String = "",
                var from: String = "", var to: String = "",
                var duration: String = "", var uri: String = "",
                var currentProgress: String = "", var isPlaying: Boolean = false)