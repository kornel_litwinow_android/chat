package kornel.litwinow.chat.model

import android.os.Build
import android.text.Html
import android.text.Spanned


object HtmlHelper {
     fun fixText(text: String): Spanned? {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(text, Html.FROM_HTML_MODE_COMPACT)
        } else {
            Html.fromHtml(text)
        }
    }
}