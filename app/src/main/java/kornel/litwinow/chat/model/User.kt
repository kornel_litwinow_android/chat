package kornel.litwinow.chat.model

import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
data class User(val id: String = "", val username: String = "",
                val lastname: String = "", val email: String = "",
                val imageURL: String = "", val status: String = "",
                val search: String = "", val company: String = "")