package kornel.litwinow.chat.model

import com.google.firebase.database.IgnoreExtraProperties
import com.google.gson.annotations.SerializedName

@IgnoreExtraProperties
data class Person(val id: String = "", val admin: String = "", val nameGroup: String = "")
