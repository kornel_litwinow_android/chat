package kornel.litwinow.chat.model

import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
class GroupMessages(val sender: String = "", val receivers: List<String> = listOf(),
                    val message: String = "", val isseen: Boolean = false, val timestamp: String = "", val messageType: String = "", val imagePath: String = "")