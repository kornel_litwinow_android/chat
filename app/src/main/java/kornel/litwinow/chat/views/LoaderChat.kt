package kornel.litwinow.chat.views

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import kornel.litwinow.chat.R

class LoaderChat : FrameLayout {
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context,attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context,attrs,defStyleAttr)

    init {
        LayoutInflater.from(context).inflate(R.layout.view_loader,this,true)
    }

}