package kornel.litwinow.chat.base


import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.firebase.database.FirebaseDatabase
import kornel.litwinow.chat.activities.NoInternetConnectionActivity
import kornel.litwinow.chat.helpers.ConstHelper
import kornel.litwinow.chat.helpers.AuthHelper
import kornel.litwinow.chat.viewmodels.BaseViewModel

open class BaseActivity : AppCompatActivity() {

    lateinit var baseViewModel: BaseViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        baseViewModel = ViewModelProvider(this).get(BaseViewModel::class.java)
        observeViewModel()
    }

    private fun observeViewModel() {
        baseViewModel.internetStatus.observe(this, Observer {
            if (!it) {
                this.finish()
                startActivity(NoInternetConnectionActivity.prepareInternet(this))
            }
        })
    }

    open fun status(status: String) {
        if (AuthHelper.auth?.uid != null) {
            val reference = FirebaseDatabase.getInstance().getReference(ConstHelper.USERS).child(AuthHelper.auth?.uid.toString())
            val hashMap = HashMap<String, Any>()
            hashMap[ConstHelper.STATUS] = status
            reference.updateChildren(hashMap)
        }
    }


    override fun onResume() {
        super.onResume()
//        baseViewModel.checkInternetConnection()
        if (AuthHelper.auth != null) {
            status("online")
        }
    }

    override fun onPause() {
        super.onPause()
        if (AuthHelper.auth != null) {
            status("offline")
        }
    }

}