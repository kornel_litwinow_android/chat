package kornel.litwinow.chat.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.bumptech.glide.RequestManager
import kornel.litwinow.chat.model.User
import kornel.litwinow.chat.R
import kornel.litwinow.chat.viewholders.OnlineUserViewHolder
import kornel.litwinow.chat.viewholders.UserListener
import kornel.litwinow.chat.viewholders.UserV2ViewHolder

class OnlineUserAdapter(val glide: RequestManager, val listener: UserListener) : ListAdapter<User, OnlineUserViewHolder>(DIFF_CALLBACK) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OnlineUserViewHolder =
            OnlineUserViewHolder(glide, LayoutInflater.from(parent.context).inflate(R.layout.online_user_item, parent, false))

    override fun onBindViewHolder(holder: OnlineUserViewHolder, position: Int) =
            holder.bind(getItem(position), listener)

}

private val DIFF_CALLBACK: DiffUtil.ItemCallback<User> = object : DiffUtil.ItemCallback<User>() {
    override fun areItemsTheSame(oldItem: User, newItem: User): Boolean =
            oldItem == newItem

    override fun areContentsTheSame(oldItem: User, newItem: User): Boolean =
            oldItem == newItem

}