package kornel.litwinow.chat.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestManager
import kornel.litwinow.chat.model.GroupMessages
import kornel.litwinow.chat.R
import kornel.litwinow.chat.helpers.AuthHelper
import kornel.litwinow.chat.viewholders.GroupMessageLeftV2ViewHolder
import kornel.litwinow.chat.viewholders.GroupMessageRightV2ViewHolder


private const val MSG_TYPE_LEFT = 0
private const val MSG_TYPE_RIGHT = 1

class GroupMessageV2Adapter(val glide: RequestManager, val imageurl: String?) : ListAdapter<GroupMessages, RecyclerView.ViewHolder>(DIFF_CALLBACK) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
            if (viewType == MSG_TYPE_RIGHT) {
                GroupMessageRightV2ViewHolder(glide, LayoutInflater.from(parent.context).inflate(R.layout.chat_item_right, parent, false))
            } else {
                GroupMessageLeftV2ViewHolder(glide, LayoutInflater.from(parent.context).inflate(R.layout.chat_item_left, parent, false))
            }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = getItem(position)
        when (holder) {
            is GroupMessageLeftV2ViewHolder -> holder.bind(item,imageurl,position,itemCount)
            is GroupMessageRightV2ViewHolder -> holder.bind(item,imageurl,position,itemCount)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (getItem(position).sender == AuthHelper.auth?.currentUser?.uid) {
            MSG_TYPE_RIGHT
        } else {
            MSG_TYPE_LEFT
        }
    }
}

private val DIFF_CALLBACK: DiffUtil.ItemCallback<GroupMessages> = object : DiffUtil.ItemCallback<GroupMessages>() {
    override fun areItemsTheSame(oldItem: GroupMessages, newItem: GroupMessages): Boolean =
            oldItem == newItem

    override fun areContentsTheSame(oldItem: GroupMessages, newItem: GroupMessages): Boolean =
            true

}