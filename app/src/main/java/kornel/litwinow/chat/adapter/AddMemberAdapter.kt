package kornel.litwinow.chat.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.bumptech.glide.RequestManager
import kornel.litwinow.chat.R
import kornel.litwinow.chat.model.User
import kornel.litwinow.chat.viewholders.AddMemberViewHolder
import kornel.litwinow.chat.viewholders.AddMemberViewHolder.*

class AddMemberAdapter(val glide: RequestManager, private val listener: AddRemoveMemberListener) : ListAdapter<User, AddMemberViewHolder>(DIFF_CALLBACK) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AddMemberViewHolder =
            AddMemberViewHolder(glide, LayoutInflater.from(parent.context).inflate(R.layout.add_remove_member_item, parent, false))

    override fun onBindViewHolder(holder: AddMemberViewHolder, position: Int) =
            holder.bind(getItem(position), listener)

}

private val DIFF_CALLBACK: DiffUtil.ItemCallback<User> = object : DiffUtil.ItemCallback<User>() {
    override fun areItemsTheSame(oldItem: User, newItem: User): Boolean =
            oldItem == newItem

    override fun areContentsTheSame(oldItem: User, newItem: User): Boolean =
            oldItem == newItem

}