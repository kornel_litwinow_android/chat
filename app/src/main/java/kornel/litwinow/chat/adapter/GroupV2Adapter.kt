package kornel.litwinow.chat.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.bumptech.glide.RequestManager
import kornel.litwinow.chat.model.Group
import kornel.litwinow.chat.R
import kornel.litwinow.chat.viewholders.GroupListener
import kornel.litwinow.chat.viewholders.GroupV2ViewHolder

class GroupV2Adapter(val glide: RequestManager,val listener: GroupListener) : ListAdapter<Group,GroupV2ViewHolder>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GroupV2ViewHolder =
            GroupV2ViewHolder(glide,LayoutInflater.from(parent.context).inflate(R.layout.group_item,parent,false))

    override fun onBindViewHolder(holder: GroupV2ViewHolder, position: Int) = holder.bind(getItem(position),listener)

}

private val DIFF_CALLBACK: DiffUtil.ItemCallback<Group> = object : DiffUtil.ItemCallback<Group>(){
    override fun areItemsTheSame(oldItem: Group, newItem: Group): Boolean =
            oldItem.groupName == newItem.groupName

    override fun areContentsTheSame(oldItem: Group, newItem: Group): Boolean =
            oldItem.groupName == newItem.groupName

}

