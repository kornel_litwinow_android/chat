package kornel.litwinow.chat.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.bumptech.glide.RequestManager
import kornel.litwinow.chat.model.User
import kornel.litwinow.chat.R
import kornel.litwinow.chat.viewholders.UserListener
import kornel.litwinow.chat.viewholders.UserV2ViewHolder

class UserV2Adapter(val glide: RequestManager, val listener: UserListener, var isChat: Boolean, var isHideLastMessage: Boolean) : ListAdapter<User, UserV2ViewHolder>(DIFF_CALLBACK) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserV2ViewHolder =
            UserV2ViewHolder(glide, LayoutInflater.from(parent.context).inflate(R.layout.user_item, parent, false))

    override fun onBindViewHolder(holder: UserV2ViewHolder, position: Int) =
            holder.bind(getItem(position), listener, isChat, isHideLastMessage)

}

private val DIFF_CALLBACK: DiffUtil.ItemCallback<User> = object : DiffUtil.ItemCallback<User>() {
    override fun areItemsTheSame(oldItem: User, newItem: User): Boolean =
            oldItem == newItem

    override fun areContentsTheSame(oldItem: User, newItem: User): Boolean =
            oldItem == newItem

}