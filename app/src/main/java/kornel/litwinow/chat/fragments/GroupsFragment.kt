package kornel.litwinow.chat.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetDialog
import dagger.hilt.android.AndroidEntryPoint
import kornel.litwinow.chat.activities.GroupMessageActivity
import kornel.litwinow.chat.adapter.GroupV2Adapter
import kornel.litwinow.chat.databinding.DialogAddGroupBinding
import kornel.litwinow.chat.databinding.FragmentGroupsBinding
import kornel.litwinow.chat.helpers.gone
import kornel.litwinow.chat.helpers.show
import kornel.litwinow.chat.model.Group
import kornel.litwinow.chat.viewholders.GroupListener
import kornel.litwinow.chat.viewmodels.MainActivityViewModel
import kornel.litwinow.chat.viewmodels.MainActivityViewModel.MainActivityViewState.*

@AndroidEntryPoint
class GroupsFragment : Fragment() {

    private lateinit var binding: FragmentGroupsBinding
    private lateinit var bindingDialog: DialogAddGroupBinding
    private lateinit var viewModel: MainActivityViewModel

    private val groupV2Adapter by lazy {
        GroupV2Adapter(Glide.with(this), object : GroupListener {
            override fun goMessagesGroup(nameGroup: String) {
                context?.startActivity(GroupMessageActivity.prepareIntent(requireContext(), nameGroup))
            }
        })
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = FragmentGroupsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(MainActivityViewModel::class.java)
        binding.fab.setOnClickListener {
            dialogAddGroup()
        }
        viewModel.getGroups()
        observeViewModel()
        binding.recyclerView.adapter = groupV2Adapter
    }

    private fun observeViewModel() {
        viewModel.liveDataMainActivityViewState.observe(viewLifecycleOwner, Observer {
            when (it) {
                is AddGroupSuccess -> Toast.makeText(context, "Created new group", Toast.LENGTH_SHORT).show()
                is AddGroupFailed -> Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show()
                is Groups -> showGroups(it.groupList)
                is EmptyList -> binding.lottieAnimation.show()
            }
        })
    }

    private fun showGroups(groupList: List<Group>) {
        groupV2Adapter.submitList(groupList)
        groupV2Adapter.notifyDataSetChanged()
        binding.lottieAnimation.gone()
    }

    private fun dialogAddGroup() {
        val dialog = context?.let { BottomSheetDialog(it) }
        bindingDialog = DialogAddGroupBinding.inflate(layoutInflater)
        val view = bindingDialog.root
        bindingDialog.addGroup.setOnClickListener {
            viewModel.createGroup(bindingDialog.nameGroup.text.toString())
            dialog?.dismiss()
        }
        dialog?.setContentView(view)
        dialog?.show()
    }

}
