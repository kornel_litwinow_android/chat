package kornel.litwinow.chat.fragments

import kornel.litwinow.chat.notifications.MyResponseV2
import kornel.litwinow.chat.notifications.SenderV2
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

interface ApiV2Service {

    @Headers("Content-Type:application/json", "Authorization:key=AAAAAMJh02w:APA91bEnMD7L3Mrhb44XD3N-jq4K9uZzKuIKTjdMNobr_lrKge20ZokI9UFcItNgUgENTwLs_T0aYE4d-sQFNf9h9TFGy8nQb2aQTQEVEnD5UUSPxlTYwdPUF0xXZE-f_0R85wyPcKbv")
    @POST("fcm/send")
    fun sendNotification(@Body body: SenderV2): Call<MyResponseV2>
}