package kornel.litwinow.chat.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import dagger.hilt.android.AndroidEntryPoint
import kornel.litwinow.chat.activities.MessageActivity
import kornel.litwinow.chat.adapter.OnlineUserAdapter
import kornel.litwinow.chat.databinding.FragmentOnlineUsersBinding
import kornel.litwinow.chat.helpers.gone
import kornel.litwinow.chat.helpers.show
import kornel.litwinow.chat.viewholders.UserListener
import kornel.litwinow.chat.viewmodels.MainActivityViewModel
import kornel.litwinow.chat.viewmodels.MainActivityViewModel.MainActivityViewState.*

@AndroidEntryPoint
class OnlineUsersFragment : Fragment() {

    private lateinit var binding: FragmentOnlineUsersBinding
    private lateinit var viewModel: MainActivityViewModel

    private val userV2Adapter by lazy {
        OnlineUserAdapter(Glide.with(this), object : UserListener {
            override fun goUserMessage(userId: String) {
                context?.startActivity(MessageActivity.prepareIntent(requireContext(), userId))
            }

        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentOnlineUsersBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(MainActivityViewModel::class.java)
        viewModel.getOnlineUsers()

        observeViewModel()
        binding.recyclerView.adapter = userV2Adapter
    }


    private fun observeViewModel() {
        viewModel.liveDataMainActivityViewState.observe(viewLifecycleOwner, Observer {
            when (it) {
                is OnlineUsers -> {
                    userV2Adapter.submitList(it.userList)
                    userV2Adapter.notifyDataSetChanged()
                    binding.lottieAnimation.gone()
                }

                is EmptyList -> {
                    userV2Adapter.submitList(listOf())
                    userV2Adapter.notifyDataSetChanged()
                    binding.lottieAnimation.show()
                }
            }
        })
    }

}