package kornel.litwinow.chat.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import dagger.hilt.android.AndroidEntryPoint
import kornel.litwinow.chat.activities.MessageActivity
import kornel.litwinow.chat.adapter.UserV2Adapter
import kornel.litwinow.chat.databinding.FragmentChatsBinding
import kornel.litwinow.chat.helpers.gone
import kornel.litwinow.chat.helpers.show
import kornel.litwinow.chat.model.User
import kornel.litwinow.chat.viewholders.UserListener
import kornel.litwinow.chat.viewmodels.MainActivityViewModel
import kornel.litwinow.chat.viewmodels.MainActivityViewModel.MainActivityViewState.*

@AndroidEntryPoint
class ChatsFragment : Fragment() {

    private lateinit var viewModel: MainActivityViewModel
    private lateinit var binding: FragmentChatsBinding

    private val userV2Adapter by lazy {
        UserV2Adapter(Glide.with(this), object : UserListener {
            override fun goUserMessage(userId: String) {
                context?.startActivity(MessageActivity.prepareIntent(requireContext(), userId))
            }
        }, isChat = true, isHideLastMessage = false)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentChatsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(MainActivityViewModel::class.java)
        viewModel.updateToken()
        viewModel.initChatList()
        observeViewModel()
        binding.recyclerView.adapter = userV2Adapter
    }

    private fun observeViewModel() {
        viewModel.liveDataMainActivityViewState.observe(viewLifecycleOwner, Observer {
            when (it) {
                is InitUserList -> showUserChat(it.chatList)
                is EmptyList -> binding.lottieAnimation.show()
            }
        })
    }

    private fun showUserChat(chatList: List<User>) {
        userV2Adapter.submitList(chatList)
        userV2Adapter.notifyDataSetChanged()
        binding.lottieAnimation.gone()
    }

}
