package kornel.litwinow.chat.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import dagger.hilt.android.AndroidEntryPoint
import kornel.litwinow.chat.databinding.ActivityNoInternetConnectionBinding
import kornel.litwinow.chat.viewmodels.BaseViewModel

@AndroidEntryPoint
class NoInternetConnectionActivity : AppCompatActivity() {

    private var _binding: ActivityNoInternetConnectionBinding? = null
    private val binding get() = _binding!!
    private lateinit var viewModel: BaseViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityNoInternetConnectionBinding.inflate(layoutInflater)
        setContentView(binding.root)
        viewModel = ViewModelProvider(this).get(BaseViewModel::class.java)

        binding.btnGoSettings.setOnClickListener {
            val intent = Intent()
            intent.action = Settings.ACTION_WIFI_SETTINGS
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        }

        viewModel.checkInternetConnection()
    }

    override fun onResume() {
        super.onResume()
        viewModel.internetStatus.observe(this, Observer {
            if (it) {
                this.finish()
                startActivity(SplashScreenActivity.prepareIntent(this))
            }
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    companion object {
        fun prepareInternet(context: Context) = Intent(context, NoInternetConnectionActivity::class.java)
    }
}