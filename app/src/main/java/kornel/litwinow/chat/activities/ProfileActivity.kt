package kornel.litwinow.chat.activities

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import dagger.hilt.android.AndroidEntryPoint
import kornel.litwinow.chat.R
import kornel.litwinow.chat.authorization.ChangePasswordActivity
import kornel.litwinow.chat.authorization.LoginActivity
import kornel.litwinow.chat.helpers.ConstHelper
import kornel.litwinow.chat.databinding.ActivityProfileBinding
import kornel.litwinow.chat.helpers.gone
import kornel.litwinow.chat.helpers.invisible
import kornel.litwinow.chat.helpers.show
import kornel.litwinow.chat.helpers.AuthHelper
import kornel.litwinow.chat.model.User
import kornel.litwinow.chat.viewmodels.ProfileViewModel
import kornel.litwinow.chat.viewmodels.ProfileViewModel.ProfileViewState.*
import java.util.*

private const val IMAGE_REQUEST = 100

@AndroidEntryPoint
class ProfileActivity : AppCompatActivity() {

    private var _binding: ActivityProfileBinding? = null
    private val binding get() = _binding!!
    private lateinit var viewModel: ProfileViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityProfileBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        viewModel = ViewModelProvider(this).get(ProfileViewModel::class.java)
        initToolbar()
        viewModel.getUserInfo()
        binding.profileImage.setOnClickListener { openImage() }
        binding.btnLogout.setOnClickListener { logOut() }
        binding.btnChangePassword.setOnClickListener { startActivity(ChangePasswordActivity.prepareIntent(this)) }
        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel.liveDataProfileViewState.observe(this, Observer {
            when (it) {
                is Loading -> showLoading()
                is Error -> showError()
                is Success -> showUserInfo(it.user)
                is ProgressUpdateImage -> showLoading()
                is SuccessUpdateImage -> uploadImageSuccess()
                is FailedUpdateImage -> showError()
            }
        })
    }

    private fun uploadImageSuccess() {
        binding.loader.gone()
        binding.widgetGroup.show()
        Toast.makeText(this, getString(R.string.photo_was_updated), Toast.LENGTH_SHORT).show()
    }

    private fun showUserInfo(user: User?) {
        val name = user?.username
        val upperName = name?.substring(0, 1)?.toUpperCase(Locale.getDefault()) + name?.substring(1)
        val lastName = user?.lastname
        val upperLastName = lastName?.substring(0, 1)?.toUpperCase(Locale.getDefault()) + lastName?.substring(1)
        binding.txtEmail.text = user?.email
        binding.txtNameLastname.text = getString(R.string.profile_name_lastname, upperName, upperLastName)
        if (user?.imageURL.equals(ConstHelper.DEFAULT)) {
            binding.profileImage.setImageResource(R.mipmap.ic_person_placeholder)
        } else {
            Glide.with(applicationContext).load(user?.imageURL).placeholder(R.mipmap.ic_person_placeholder).into(binding.profileImage)
        }
        binding.loader.gone()
        binding.widgetGroup.show()
    }


    private fun showError() {
        binding.loader.gone()
        binding.widgetGroup.show()
        Toast.makeText(this, getString(R.string.something_gone_wrong), Toast.LENGTH_SHORT).show()
    }

    private fun showLoading() {
        binding.loader.show()
        binding.widgetGroup.invisible()
    }

    private fun logOut() {
        AuthHelper.auth?.signOut()
        finish()
        val intent = LoginActivity.prepareIntent(this)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
    }

    private fun openImage() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(intent, IMAGE_REQUEST)
    }

    private fun initToolbar() {
        setSupportActionBar(binding.toolbar)
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setHomeButtonEnabled(true)
            setDisplayShowTitleEnabled(true)
            setHomeAsUpIndicator(R.drawable.ic_back_dark)
            binding.toolbar.setNavigationOnClickListener { finish() }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == IMAGE_REQUEST && resultCode == Activity.RESULT_OK && data != null && data.data != null) {
            viewModel.uploadImage(data.data, this.contentResolver)

        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    companion object {
        fun prepareIntent(context: Context) = Intent(context, ProfileActivity::class.java)
    }

}