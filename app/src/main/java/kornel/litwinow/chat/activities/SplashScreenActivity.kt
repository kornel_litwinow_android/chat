package kornel.litwinow.chat.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import dagger.hilt.android.AndroidEntryPoint
import kornel.litwinow.chat.authorization.LoginActivity
import kornel.litwinow.chat.R
import kornel.litwinow.chat.helpers.AuthHelper

@AndroidEntryPoint
class SplashScreenActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme_Launcher)
        super.onCreate(savedInstanceState)
        AuthHelper.getUserAuth()
        runToNextScreen()
    }

    private fun runToNextScreen() {
        if (AuthHelper.auth?.currentUser != null) {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        } else {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }
        this.finish()
    }

    companion object {
        fun prepareIntent(context: Context) = Intent(context, SplashScreenActivity::class.java)
    }
}