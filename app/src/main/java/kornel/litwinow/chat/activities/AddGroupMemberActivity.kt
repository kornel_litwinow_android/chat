package kornel.litwinow.chat.activities

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import dagger.hilt.android.AndroidEntryPoint
import kornel.litwinow.chat.model.User
import kornel.litwinow.chat.R
import kornel.litwinow.chat.adapter.AddMemberAdapter
import kornel.litwinow.chat.base.BaseActivity
import kornel.litwinow.chat.helpers.ConstHelper
import kornel.litwinow.chat.databinding.ActivityAddMemberGroupBinding
import kornel.litwinow.chat.helpers.gone
import kornel.litwinow.chat.helpers.invisible
import kornel.litwinow.chat.helpers.show
import kornel.litwinow.chat.viewholders.AddMemberViewHolder
import kornel.litwinow.chat.viewmodels.AddGroupMemberViewModel
import kornel.litwinow.chat.viewmodels.AddGroupMemberViewModel.AddGroupMemberViewState.*

@AndroidEntryPoint
class AddGroupMemberActivity : BaseActivity() {

    private var _binding: ActivityAddMemberGroupBinding? = null
    private val binding get() = _binding!!
    private lateinit var viewModel: AddGroupMemberViewModel

    private var name: String = ""

    private val userV2Adapter by lazy {
        AddMemberAdapter(Glide.with(this), object : AddMemberViewHolder.AddRemoveMemberListener {
            override fun addOrRemoveMember(userId: String) {
                viewModel.addMemberToPerson(userId, name)
                viewModel.getUsers(binding.searchUsers.text.toString(), name)
                Toast.makeText(this@AddGroupMemberActivity, "Added member to group $name", Toast.LENGTH_SHORT).show()
            }
        })
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityAddMemberGroupBinding.inflate(layoutInflater)
        setContentView(binding.root)
        viewModel = ViewModelProvider(this).get(AddGroupMemberViewModel::class.java)
        intent.extras.apply {
            name = intent.getStringExtra(ConstHelper.NAME_GROUP) as String
        }
        initToolbar()
        viewModel.getUsers(binding.searchUsers.text.toString(), name)
        searchUserTextListener()
        observeViewModel()
        searchUserTextListener()
    }

    private fun observeViewModel() {
        viewModel.liveDataMemberViewState.observe(this, Observer {
            when (it) {
                is Loading -> showLoading()
                is Success -> showUsers(it.userList)
                is Error -> showError()
            }
        })
    }

    override fun onResume() {
        super.onResume()
        viewModel.getUsers(binding.searchUsers.text.toString(), name)
    }

    private fun showError() {
        binding.loader.gone()
        binding.widgetGroup.show()
        Toast.makeText(this, "Error connecting", Toast.LENGTH_SHORT).show()
    }

    private fun showUsers(userList: List<User>) {
        with(binding) {
            loader.gone()
            widgetGroup.show()
            userV2Adapter.submitList(userList)
            recyclerView.adapter = userV2Adapter
        }
    }

    private fun showLoading() {
        binding.loader.show()
        binding.widgetGroup.invisible()
    }

    private fun searchUserTextListener() {
        binding.searchUsers.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            @SuppressLint("DefaultLocale")
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                viewModel.searchUsers(s.toString().toLowerCase())
            }

        })
    }

    private fun initToolbar() {
        setSupportActionBar(binding.toolbar)
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setHomeButtonEnabled(true)
            setDisplayShowTitleEnabled(true)
            title = "Add member"
            setHomeAsUpIndicator(R.drawable.ic_back)
        }
        binding.toolbar.setNavigationOnClickListener { finish() }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    companion object {
        fun prepareIntent(context: Context, nameGroup: String): Intent {
            val intent = Intent(context, AddGroupMemberActivity::class.java)
            intent.putExtra(ConstHelper.NAME_GROUP, nameGroup)
            return intent
        }
    }
}