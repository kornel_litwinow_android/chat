package kornel.litwinow.chat.activities

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import dagger.hilt.android.AndroidEntryPoint
import kornel.litwinow.chat.adapter.UserV2Adapter
import kornel.litwinow.chat.model.User
import kornel.litwinow.chat.R
import kornel.litwinow.chat.base.BaseActivity
import kornel.litwinow.chat.databinding.ActivitySearchBinding
import kornel.litwinow.chat.helpers.gone
import kornel.litwinow.chat.helpers.show
import kornel.litwinow.chat.viewholders.UserListener
import kornel.litwinow.chat.viewmodels.SearchViewModel
import kotlinx.android.synthetic.main.activity_search.*

@AndroidEntryPoint
class SearchActivity : BaseActivity() {

    private var _binding: ActivitySearchBinding? = null
    private val binding get() = _binding!!
    private lateinit var viewModel: SearchViewModel

    private val userV2Adapter by lazy {
        UserV2Adapter(Glide.with(this), object : UserListener {
            override fun goUserMessage(userId: String) {
                startActivity(MessageActivity.prepareIntent(this@SearchActivity, userId))
            }

        }, isChat = false, isHideLastMessage = true)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivitySearchBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        viewModel = ViewModelProvider(this).get(SearchViewModel::class.java)
        initToolbar()
        viewModel.getUsers(binding.searchUsers.text.toString())
        searchUserTextListener()
        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel.liveDataSearchViewState.observe(this, Observer {
            when (it) {
                is SearchViewModel.SearchViewState.Loading -> showLoading()
                is SearchViewModel.SearchViewState.Success -> showUsers(it.userList)
                is SearchViewModel.SearchViewState.Error -> showError()
            }
        })
    }

    override fun onResume() {
        super.onResume()
        viewModel.getUsers(binding.searchUsers.text.toString())
    }

    private fun showError() {
        binding.loader.visibility = View.GONE
        binding.widgetGroup.visibility = View.VISIBLE
        Toast.makeText(this, "Error connecting", Toast.LENGTH_SHORT).show()
    }

    private fun showUsers(userList: List<User>) {
        binding.loader.gone()
        binding.widgetGroup.show()
        userV2Adapter.submitList(userList)
        binding.recyclerView.adapter = userV2Adapter
    }

    private fun showLoading() {
        binding.loader.visibility = View.VISIBLE
        binding.widgetGroup.visibility = View.INVISIBLE
    }

    private fun initToolbar() {
        setSupportActionBar(binding.toolbar)
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setHomeButtonEnabled(true)
            setDisplayShowTitleEnabled(true)
            binding.toolbar.setNavigationOnClickListener { finish() }
            title = "Search users"
            setHomeAsUpIndicator(R.drawable.ic_back)
        }
    }

    private fun searchUserTextListener() {
        binding.searchUsers.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            @SuppressLint("DefaultLocale")
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                viewModel.searchUsers(s.toString().toLowerCase())
            }

        })
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    companion object {
        fun prepareIntent(context: Context) = Intent(context, SearchActivity::class.java)
    }
}