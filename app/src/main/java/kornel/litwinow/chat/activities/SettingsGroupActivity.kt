package kornel.litwinow.chat.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import dagger.hilt.android.AndroidEntryPoint
import kornel.litwinow.chat.R
import kornel.litwinow.chat.base.BaseActivity
import kornel.litwinow.chat.databinding.ActivitySettingsGroupBinding
import kornel.litwinow.chat.helpers.ConstHelper

@AndroidEntryPoint
class SettingsGroupActivity : BaseActivity() {

    private var _binding: ActivitySettingsGroupBinding? = null
    private val binding get() = _binding!!
    private var name: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivitySettingsGroupBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        initToolbar()
        intent.extras.apply {
            name = intent.getStringExtra(ConstHelper.NAME_GROUP) as String
        }

        binding.addMemberToGroup.setOnClickListener { startActivity(AddGroupMemberActivity.prepareIntent(this, name)) }
        binding.removeMemberFromGroup.setOnClickListener { startActivity(RemoveMemberFromGroupActivity.prepareIntent(this, name)) }
    }

    private fun initToolbar() {
        setSupportActionBar(binding.toolbar)
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setHomeButtonEnabled(true)
            setDisplayShowTitleEnabled(true)
            binding.toolbar.setNavigationOnClickListener { finish() }
            title = "Settings Group"
            setHomeAsUpIndicator(R.drawable.ic_back)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    companion object {
        fun prepareIntent(context: Context, nameGroup: String): Intent {
            val intent = Intent(context, SettingsGroupActivity::class.java)
            intent.putExtra(ConstHelper.NAME_GROUP, nameGroup)
            return intent
        }
    }

}