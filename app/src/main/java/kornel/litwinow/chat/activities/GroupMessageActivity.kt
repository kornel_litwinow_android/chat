package kornel.litwinow.chat.activities

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.webkit.MimeTypeMap
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import dagger.hilt.android.AndroidEntryPoint
import kornel.litwinow.chat.adapter.GroupMessageV2Adapter
import kornel.litwinow.chat.model.GroupMessages
import kornel.litwinow.chat.R
import kornel.litwinow.chat.base.BaseActivity
import kornel.litwinow.chat.helpers.ConstHelper
import kornel.litwinow.chat.databinding.ActivityGroupMessageBinding
import kornel.litwinow.chat.viewmodels.GroupMessageViewModel
import kornel.litwinow.chat.viewmodels.GroupMessageViewModel.GroupMessageViewState.*

private const val RC_SELECT_IMAGE = 2

@AndroidEntryPoint
class GroupMessageActivity : BaseActivity() {

    private var _binding: ActivityGroupMessageBinding? = null
    private val binding get() = _binding!!
    private lateinit var viewModel: GroupMessageViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityGroupMessageBinding.inflate(layoutInflater)
        setContentView(binding.root)
        viewModel = ViewModelProvider(this).get(GroupMessageViewModel::class.java)
        viewModel.nameGroup = intent.getStringExtra(ConstHelper.GROUP_NAME) as String
        binding.username.text = viewModel.nameGroup
        initToolbar()
        viewModel.getUsersToTalk()
        btnSendMessage()
        binding.sendImage.setOnClickListener {
            openImage()
        }
        observeViewModel()
        viewModel.seenMessage()
    }

    private fun openImage() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(intent, RC_SELECT_IMAGE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SELECT_IMAGE && resultCode == RESULT_OK && data != null && data.data != null) {
            val imageUri = data.data
            viewModel.uploadImage(imageUri, getFileExtension(imageUri!!).toString())
        }
    }

    private fun getFileExtension(uri: Uri): String? {
        val contentResolver = this.contentResolver
        val mimeTypeMap = MimeTypeMap.getSingleton()
        return mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(uri))
    }

    private fun observeViewModel() {
        viewModel.messageViewState.observe(this, Observer {
            when (it) {
                is Messages -> showMessages(it.listMessages)
                is HideLoadingImage -> binding.loader.visibility = View.GONE
                is LoadingImage -> binding.loader.visibility = View.VISIBLE
                is FailUpdateImage -> {
                    binding.loader.visibility = View.GONE
                    Toast.makeText(this, "Failed sending image", Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    private fun showMessages(listMessages: List<GroupMessages>) {
        val adapter = GroupMessageV2Adapter(Glide.with(this), "default")
        adapter.submitList(listMessages)
        adapter.notifyDataSetChanged()
        val linearLayoutManager = LinearLayoutManager(this)
        linearLayoutManager.stackFromEnd = true
        binding.recyclerView.layoutManager = linearLayoutManager
        binding.recyclerView.adapter = adapter
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_group_message, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.add_member) {
            startActivity(SettingsGroupActivity.prepareIntent(context = this, nameGroup = viewModel.nameGroup))
            return true
        }
        return false
    }

    private fun initToolbar() {
        setSupportActionBar(binding.toolbar)
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setHomeButtonEnabled(true)
            setDisplayShowTitleEnabled(false)
            setHomeAsUpIndicator(R.drawable.ic_back)
            binding.toolbar.setNavigationOnClickListener { finish() }
        }
    }


    private fun btnSendMessage() {
        binding.buttonSend.setOnClickListener {
            val msg = binding.textMessage.text.toString()
            if (msg != "") {
                viewModel.sendMessage(msg, "")
            } else {
                Toast.makeText(this, "You can't send empty message", Toast.LENGTH_SHORT).show()
            }
            binding.textMessage.setText("")
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    companion object {
        fun prepareIntent(context: Context, groupName: String): Intent {
            val intent = Intent(context, GroupMessageActivity::class.java)
            intent.putExtra(ConstHelper.GROUP_NAME, groupName)
            return intent
        }
    }

}
