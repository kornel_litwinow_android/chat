package kornel.litwinow.chat.activities

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import kornel.litwinow.chat.R
import kornel.litwinow.chat.adapter.AddMemberAdapter
import kornel.litwinow.chat.databinding.ActivityRemoveMemberFromGroupBinding
import kornel.litwinow.chat.helpers.ConstHelper
import kornel.litwinow.chat.helpers.gone
import kornel.litwinow.chat.helpers.invisible
import kornel.litwinow.chat.helpers.show
import kornel.litwinow.chat.model.User
import kornel.litwinow.chat.viewholders.AddMemberViewHolder
import kornel.litwinow.chat.viewmodels.RemoveGroupMemberViewModel
import kornel.litwinow.chat.viewmodels.RemoveGroupMemberViewModel.AddGroupMemberViewState
import kornel.litwinow.chat.viewmodels.RemoveGroupMemberViewModel.AddGroupMemberViewState.*

class RemoveMemberFromGroupActivity : AppCompatActivity() {

    private var _binding: ActivityRemoveMemberFromGroupBinding? = null
    private val binding get() = _binding!!
    private lateinit var viewModel: RemoveGroupMemberViewModel


    private val userV2Adapter by lazy {
        AddMemberAdapter(Glide.with(this), object : AddMemberViewHolder.AddRemoveMemberListener {
            override fun addOrRemoveMember(userId: String) {
                viewModel.removeMemberToPerson(userId, name)
                viewModel.getUsers(binding.searchUsers.text.toString(), name)
                showUsers(viewModel.userList2)
                Toast.makeText(this@RemoveMemberFromGroupActivity, "Remove member from group $name", Toast.LENGTH_SHORT).show()
            }
        })
    }

    private var name: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityRemoveMemberFromGroupBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        viewModel = ViewModelProvider(this).get(RemoveGroupMemberViewModel::class.java)
        intent.extras.apply {
            name = intent.getStringExtra(ConstHelper.NAME_GROUP) as String
        }
        initToolbar()
        viewModel.getUsers(binding.searchUsers.text.toString(), name)
        searchUserTextListener()
        observeViewModel()
        searchUserTextListener()
    }

    private fun observeViewModel() {
        viewModel.liveDataMemberViewState.observe(this, Observer {
            when (it) {
                is Loading -> showLoading()
                is Success -> showUsers(it.userList)
                is Error -> showError()
            }
        })
    }

    override fun onResume() {
        super.onResume()
        viewModel.getUsers(binding.searchUsers.text.toString(), name)
    }

    private fun showError() {
        binding.loader.gone()
        binding.widgetGroup.show()
        Toast.makeText(this, "Error connecting", Toast.LENGTH_SHORT).show()
    }

    private fun showUsers(userList: List<User>) {
        binding.loader.gone()
        binding.widgetGroup.show()
        userV2Adapter.submitList(userList)
        binding.recyclerView.adapter = userV2Adapter
    }

    private fun showLoading() {
        binding.loader.show()
        binding.widgetGroup.invisible()
    }

    private fun searchUserTextListener() {
        binding.searchUsers.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            @SuppressLint("DefaultLocale")
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                viewModel.searchUsers(s.toString().toLowerCase())
            }

        })
    }

    private fun initToolbar() {
        setSupportActionBar(binding.toolbar)
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setHomeButtonEnabled(true)
            setDisplayShowTitleEnabled(true)
            title = "Remove member"
            setHomeAsUpIndicator(R.drawable.ic_back)
        }
        binding.toolbar.setNavigationOnClickListener { finish() }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    companion object {
        fun prepareIntent(context: Context, nameGroup: String): Intent {
            val intent = Intent(context, RemoveMemberFromGroupActivity::class.java)
            intent.putExtra(ConstHelper.NAME_GROUP, nameGroup)
            return intent
        }
    }
}