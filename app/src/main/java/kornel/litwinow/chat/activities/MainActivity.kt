package kornel.litwinow.chat.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View.*
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import dagger.hilt.android.AndroidEntryPoint
import kornel.litwinow.chat.fragments.ChatsFragment
import kornel.litwinow.chat.fragments.GroupsFragment
import kornel.litwinow.chat.fragments.OnlineUsersFragment
import kornel.litwinow.chat.model.User
import kornel.litwinow.chat.R
import kornel.litwinow.chat.base.BaseActivity
import kornel.litwinow.chat.databinding.ActivityMainBinding
import kornel.litwinow.chat.viewmodels.MainActivityViewModel
import kornel.litwinow.chat.viewmodels.MainActivityViewModel.MainActivityViewState.*
import kotlinx.android.synthetic.main.activity_main.*

@AndroidEntryPoint
class MainActivity : BaseActivity() {

    private lateinit var viewModel: MainActivityViewModel
    private var _binding: ActivityMainBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        viewModel = ViewModelProvider(this).get(MainActivityViewModel::class.java)

        binding.imgProfile.setOnClickListener {
            startActivity(ProfileActivity.prepareIntent(this))

        }
        binding.searchView.setOnClickListener {
            startActivity(SearchActivity.prepareIntent(this))
        }
        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel.liveDataMainActivityViewState.observe(this, Observer {
            when (it) {
                is Loading -> showLoading()
                is UserInfo -> showUserInfo(it.user)
                is PagerInfo -> initPagerFragments(it.unread)
                is Error -> showError()
            }
        })
    }

    private fun showError() {
        binding.loader.visibility = GONE
        binding.viewPager.visibility = VISIBLE
        Toast.makeText(this, "Something was wrong :(", Toast.LENGTH_SHORT).show()
    }

    private fun initPagerFragments(unread: Int) {
        binding.loader.visibility = GONE
        binding.viewPager.visibility = VISIBLE
        val viewPagerAdapter = ViewPagerAdapter(supportFragmentManager)
        if (unread == 0) {
            viewPagerAdapter.addFragment(ChatsFragment(), "Chats")
        } else {
            viewPagerAdapter.addFragment(ChatsFragment(), "Chats($unread)")
        }

        viewPagerAdapter.addFragment(OnlineUsersFragment(), "Online")
        viewPagerAdapter.addFragment(GroupsFragment(), "Groups")

        view_pager.adapter = viewPagerAdapter
        tab_layout.setupWithViewPager(view_pager)
    }


    private fun showUserInfo(user: User?) {
        val name = user?.username
        val upperName = name?.substring(0, 1)?.toUpperCase() + name?.substring(1)
        val lastName = user?.lastname
        val upperLastName = lastName?.substring(0, 1)?.toUpperCase() + lastName?.substring(1)
        txt_name.text = getString(R.string.profile_name_lastname, upperName, upperLastName)
        if (user?.imageURL.equals("default")) {
            img_profile.setImageResource(R.mipmap.ic_person_placeholder)
        } else {
            Glide.with(applicationContext).load(user?.imageURL).into(img_profile)
        }
    }

    private fun showLoading() {
        binding.loader.visibility = VISIBLE
        binding.viewPager.visibility = INVISIBLE
    }


    inner class ViewPagerAdapter(manager: FragmentManager) : FragmentPagerAdapter(manager) {

        private val fragments = mutableListOf<Fragment>()
        private val titles = mutableListOf<String>()

        override fun getItem(position: Int): Fragment = fragments[position]

        override fun getCount(): Int = fragments.size

        fun addFragment(fragment: Fragment, title: String) {
            fragments.add(fragment)
            titles.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return titles[position]
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    companion object {
        fun prepareIntent(context: Context) = Intent(context, MainActivity::class.java)
    }
}