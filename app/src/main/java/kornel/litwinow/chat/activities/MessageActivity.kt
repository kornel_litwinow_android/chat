package kornel.litwinow.chat.activities

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.inputmethod.EditorInfo
import android.webkit.MimeTypeMap
import android.widget.Toast
import android.widget.Toast.LENGTH_SHORT
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import dagger.hilt.android.AndroidEntryPoint
import kornel.litwinow.chat.R
import kornel.litwinow.chat.adapter.MessageV2Adapter
import kornel.litwinow.chat.base.BaseActivity
import kornel.litwinow.chat.helpers.ConstHelper
import kornel.litwinow.chat.databinding.ActivityMessageBinding
import kornel.litwinow.chat.helpers.gone
import kornel.litwinow.chat.helpers.show
import kornel.litwinow.chat.helpers.AuthHelper
import kornel.litwinow.chat.model.Chat
import kornel.litwinow.chat.model.User
import kornel.litwinow.chat.viewmodels.MessageViewModel
import kornel.litwinow.chat.viewmodels.MessageViewModel.MessageViewState.*
import kotlinx.android.synthetic.main.activity_message.*

private const val RC_SELECT_IMAGE = 2

@AndroidEntryPoint
class MessageActivity : BaseActivity() {

    private var _binding: ActivityMessageBinding? = null
    private val binding get() = _binding!!
    private lateinit var viewModel: MessageViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityMessageBinding.inflate(layoutInflater)
        setContentView(binding.root)
        viewModel = ViewModelProvider(this).get(MessageViewModel::class.java)
        viewModel.userId = intent.getStringExtra(ConstHelper.USER_ID) as String
        viewModel.getUserToTalk()
        initToolbar()
        observeViewModel()
        binding.sendImage.setOnClickListener {
            openImage()
        }

        binding.buttonSend.setOnClickListener { sendMessage() }
        handleActionDone()
        viewModel.seenMessage()
    }

    private fun handleActionDone() {
        binding.textMessage.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                sendMessage()
                return@setOnEditorActionListener true
            }
            return@setOnEditorActionListener false
        }
    }

    private fun observeViewModel() {
        viewModel.messageViewState.observe(this, Observer {
            when (it) {
                is UserInfo -> showReceiverInfo(it.user)
                is Messages -> showMessages(it.listMessages, it.imageURL)
                is HideLoadingImage -> binding.loader.visibility = GONE
                is LoadingImage -> binding.loader.visibility = VISIBLE
                is FailUpdateImage -> {
                    binding.loader.visibility = GONE
                    Toast.makeText(this, "Failed sending image", LENGTH_SHORT).show()
                }
                is LoadMessages -> binding.lottieAnimation.show()
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SELECT_IMAGE && resultCode == RESULT_OK && data != null && data.data != null) {
            val imageUri = data.data
            viewModel.uploadImage(imageUri, getFileExtension(imageUri!!).toString())
        }
    }

    private fun sendMessage() {
        val message = binding.textMessage.text.toString()
        if (message != "") {
            viewModel.sendMessage(AuthHelper.auth?.uid
                    ?: "", viewModel.userId, message, "", ConstHelper.TEXT)
        } else {
            Toast.makeText(this, "You can't send empty message", LENGTH_SHORT).show()
        }
        binding.textMessage.setText("")
    }

    private fun getFileExtension(uri: Uri): String? {
        val contentResolver = this.contentResolver
        val mimeTypeMap = MimeTypeMap.getSingleton()
        return mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(uri))
    }

    private fun showMessages(listMessages: List<Chat>, imageURL: String?) {
        binding.lottieAnimation.gone()
        val adapter = MessageV2Adapter(Glide.with(this), imageURL)
        adapter.submitList(listMessages)
        adapter.notifyDataSetChanged()
        val linearLayoutManager = LinearLayoutManager(this)
        linearLayoutManager.stackFromEnd = true
        binding.recyclerView.layoutManager = linearLayoutManager
        binding.recyclerView.adapter = adapter
    }

    private fun showReceiverInfo(user: User?) {
        binding.username.text = getString(R.string.profile_name_lastname, user?.username, user?.lastname)
        if (user?.imageURL == ConstHelper.DEFAULT) {
            binding.profileImage.setImageResource(R.mipmap.ic_person_placeholder)
        } else {
            Glide.with(applicationContext).load(user?.imageURL).into(binding.profileImage)
        }
    }

    private fun openImage() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(intent, RC_SELECT_IMAGE)
    }

    private fun initToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setHomeButtonEnabled(true)
            setDisplayShowTitleEnabled(false)
            setHomeAsUpIndicator(R.drawable.ic_back)
            toolbar.setNavigationOnClickListener { finish() }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    companion object {
        fun prepareIntent(context: Context, userId: String): Intent {
            val intent = Intent(context, MessageActivity::class.java)
            intent.putExtra(ConstHelper.USER_ID, userId)
            return intent
        }
    }
}