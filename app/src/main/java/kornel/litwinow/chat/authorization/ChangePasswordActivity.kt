package kornel.litwinow.chat.authorization

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import dagger.hilt.android.AndroidEntryPoint
import kornel.litwinow.chat.R
import kornel.litwinow.chat.base.BaseActivity
import kornel.litwinow.chat.databinding.ActivityChangePasswordBinding
import kornel.litwinow.chat.helpers.gone
import kornel.litwinow.chat.helpers.hideKeyboard
import kornel.litwinow.chat.helpers.invisible
import kornel.litwinow.chat.helpers.show
import kornel.litwinow.chat.viewmodels.ChangePasswordViewModel
import kornel.litwinow.chat.viewmodels.ChangePasswordViewModel.ChangePasswordViewState.*

@AndroidEntryPoint
class ChangePasswordActivity : BaseActivity() {

    private lateinit var viewModel: ChangePasswordViewModel
    private lateinit var binding: ActivityChangePasswordBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityChangePasswordBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        viewModel = ViewModelProvider(this).get(ChangePasswordViewModel::class.java)
        initToolbar()
        binding.btnChangePassword.setOnClickListener {
            if (!validatePassword()) {
                viewModel.changePassword(binding.inputOldPassword.text.toString(), binding.inputNewPassword.text.toString())
                hideKeyboard()
            }
        }
        handleActionDone()
        observeViewModel()
    }

    private fun handleActionDone() {
        binding.inputRepeatPassword.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                if (!validatePassword()) {
                    viewModel.changePassword(binding.inputOldPassword.text.toString(), binding.inputNewPassword.text.toString())
                    hideKeyboard()
                    return@setOnEditorActionListener true
                }
            }
            false
        }
    }

    private fun validatePassword(): Boolean {
        val newPassword = binding.inputNewPassword.text.toString()
        val repeatPassword = binding.inputRepeatPassword.text.toString()
        val oldPassword = binding.inputOldPassword.text.toString()
        return if (newPassword != repeatPassword) {
            binding.inputLayoutRepeatPassword.error = "Passwords are not equals"
            true
        } else if (newPassword.length < 6 && newPassword.isNotEmpty() && oldPassword.isNotEmpty() && repeatPassword.isNotEmpty()) {
            binding.inputLayoutNewPassword.error = "Password is too short"
            true
        } else if (oldPassword.isEmpty() || newPassword.isEmpty() || repeatPassword.isEmpty()) {
            Toast.makeText(this, "Fields can't be empty", Toast.LENGTH_SHORT).show()
            true
        } else {
            binding.inputLayoutOldPassword.error = null
            binding.inputLayoutNewPassword.error = null
            binding.inputLayoutRepeatPassword.error = null
            false
        }
    }

    private fun observeViewModel() {
        viewModel.liveDataLoginViewState.observe(this, Observer {
            when (it) {
                is Success -> showSuccessDialog()
                is Loading -> showLoading()
                is Error -> showError()
            }
        })
    }

    private fun showError() {
        binding.widgetGroup.show()
        binding.loader.gone()
        Toast.makeText(this, getString(R.string.fix_errors_and_try_again), Toast.LENGTH_SHORT).show()
    }

    private fun showLoading() {
        binding.widgetGroup.invisible()
        binding.loader.show()
    }

    private fun showSuccessDialog() {
        binding.loader.invisible()
        binding.widgetGroup.show()
        val builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.change_password))
        builder.setCancelable(false)
        builder.setMessage(getString(R.string.change_password_success))
        builder.setPositiveButton(android.R.string.ok) { _: DialogInterface?, _: Int -> finish() }.show()
    }

    private fun initToolbar() {
        setSupportActionBar(binding.toolbar)
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setHomeButtonEnabled(true)
            setDisplayShowTitleEnabled(false)
            setHomeAsUpIndicator(R.drawable.ic_back)
            binding.toolbar.setNavigationOnClickListener { finish() }
        }
    }

    companion object {
        fun prepareIntent(context: Context) = Intent(context, ChangePasswordActivity::class.java)
    }
}