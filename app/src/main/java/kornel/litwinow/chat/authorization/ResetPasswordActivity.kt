package kornel.litwinow.chat.authorization

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import dagger.hilt.android.AndroidEntryPoint
import kornel.litwinow.chat.R
import kornel.litwinow.chat.activities.NoInternetConnectionActivity
import kornel.litwinow.chat.databinding.ActivityResetPasswordBinding
import kornel.litwinow.chat.helpers.gone
import kornel.litwinow.chat.helpers.hideKeyboard
import kornel.litwinow.chat.helpers.invisible
import kornel.litwinow.chat.helpers.show
import kornel.litwinow.chat.viewmodels.ResetPasswordViewModel
import kornel.litwinow.chat.viewmodels.ResetPasswordViewModel.ResetPasswordViewState.*

@AndroidEntryPoint
class ResetPasswordActivity : AppCompatActivity() {

    private lateinit var viewModel: ResetPasswordViewModel
    private lateinit var binding: ActivityResetPasswordBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityResetPasswordBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        viewModel = ViewModelProvider(this).get(ResetPasswordViewModel::class.java)
        initToolbar()
        observeViewModel()
        handleActionDone()
        binding.btnReset.setOnClickListener { viewModel.resetPassword(binding.inputEmail.text.toString()) }
    }

    private fun observeViewModel() {
        viewModel.liveDataViewState.observe(this, Observer {
            when (it) {
                is Loading -> showLoading()
                is Success -> successResult()
                is Error -> showError(it.error)
            }
        })
        viewModel.internetStatus.observe(this, Observer {
            if (!it) {
                this.finish()
                startActivity(NoInternetConnectionActivity.prepareInternet(this))
            }
        })
    }

    private fun handleActionDone() {
        binding.inputEmail.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                viewModel.resetPassword(binding.inputEmail.text.toString())
                hideKeyboard()
                return@setOnEditorActionListener true
            }
            false
        }
    }

    private fun successResult() {
        finish()
        Toast.makeText(this, getString(R.string.check_your_email), Toast.LENGTH_SHORT).show()
        startActivity(LoginActivity.prepareIntent(this))
    }

    private fun showLoading() {
        binding.loader.show()
        binding.widgetGroup.invisible()
        binding.toolbar.invisible()
    }

    private fun showError(error: String?) {
        binding.loader.gone()
        binding.widgetGroup.show()
        binding.toolbar.show()
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show()
    }

    private fun initToolbar() {
        setSupportActionBar(binding.toolbar)
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setHomeButtonEnabled(true)
            setDisplayShowTitleEnabled(false)
            setHomeAsUpIndicator(R.drawable.ic_back)
            binding.toolbar.setNavigationOnClickListener { finish() }
            binding.toolbar.title = getString(R.string.reset_password)
        }
    }

    companion object {
        fun prepareIntent(context: Context) = Intent(context, ResetPasswordActivity::class.java)
    }
}