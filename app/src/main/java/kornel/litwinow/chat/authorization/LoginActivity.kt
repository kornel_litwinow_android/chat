package kornel.litwinow.chat.authorization

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import dagger.hilt.android.AndroidEntryPoint
import kornel.litwinow.chat.R
import kornel.litwinow.chat.activities.MainActivity
import kornel.litwinow.chat.activities.NoInternetConnectionActivity
import kornel.litwinow.chat.databinding.ActivityLoginBinding
import kornel.litwinow.chat.helpers.gone
import kornel.litwinow.chat.helpers.hideKeyboard
import kornel.litwinow.chat.helpers.invisible
import kornel.litwinow.chat.helpers.show
import kornel.litwinow.chat.viewmodels.LoginViewModel
import kornel.litwinow.chat.viewmodels.LoginViewModel.LoginViewState.*

@AndroidEntryPoint
class LoginActivity : AppCompatActivity() {
    private lateinit var viewModel: LoginViewModel
    private lateinit var binding: ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        viewModel = ViewModelProvider(this).get(LoginViewModel::class.java)
        btnLogin()
        goToResetPassword()
        goToRegister()
        observeViewModel()
        handleActionDone()
    }

    private fun btnLogin() {
        binding.btnLogin.setOnClickListener {
            login()
        }
    }

    private fun login() {
        val email = binding.inputEmail.text.toString()
        val password = binding.inputPassword.text.toString()
        if (TextUtils.isEmpty(email) || TextUtils.isEmpty(password)) {
            Toast.makeText(this, "All fields are required", Toast.LENGTH_SHORT).show()
        } else {
            hideKeyboard()
            viewModel.login(email, password)
        }
    }

    private fun handleActionDone() {
        binding.inputPassword.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                login()
                return@setOnEditorActionListener true
            }
            false
        }
    }

    private fun observeViewModel() {
        viewModel.liveDataLoginViewState.observe(this, Observer {
            when (it) {
                is Loading -> showLoading()
                is Success -> successResult()
                is Error -> showError()
            }
        })

        viewModel.internetStatus.observe(this, Observer {
            if (!it) {
                this.finish()
                startActivity(NoInternetConnectionActivity.prepareInternet(this))
            }
        })
    }

    private fun successResult() {
        val intent = MainActivity.prepareIntent(this)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        finish()
        startActivity(intent)
    }

    private fun showLoading() {
        binding.loader.show()
        binding.widgetGroup.invisible()
    }

    private fun showError() {
        binding.loader.gone()
        binding.widgetGroup.show()
        Toast.makeText(this, getString(R.string.authentication_failed), Toast.LENGTH_SHORT).show()
    }

    private fun goToRegister() {
        binding.btnRegister.setOnClickListener {
            startActivity(RegisterActivity.prepareIntent(this))
        }
    }

    private fun goToResetPassword() {
        binding.txtForgotPassword.setOnClickListener {
            startActivity(ResetPasswordActivity.prepareIntent(this))
        }
    }

    companion object {
        fun prepareIntent(context: Context) = Intent(context, LoginActivity::class.java)
    }

}