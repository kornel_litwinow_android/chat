package kornel.litwinow.chat.authorization

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import dagger.hilt.android.AndroidEntryPoint
import kornel.litwinow.chat.activities.MainActivity
import kornel.litwinow.chat.R
import kornel.litwinow.chat.activities.NoInternetConnectionActivity
import kornel.litwinow.chat.databinding.ActivityRegisterBinding
import kornel.litwinow.chat.helpers.gone
import kornel.litwinow.chat.helpers.hideKeyboard
import kornel.litwinow.chat.helpers.invisible
import kornel.litwinow.chat.helpers.show
import kornel.litwinow.chat.viewmodels.RegisterViewModel

@AndroidEntryPoint
class
RegisterActivity : AppCompatActivity() {

    private lateinit var viewModel: RegisterViewModel
    private lateinit var binding: ActivityRegisterBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegisterBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        viewModel = ViewModelProvider(this).get(RegisterViewModel::class.java)
        initToolbar()
        btnRegisterUser()
        observeViewModel()
        handleActionDone()
    }

    private fun btnRegisterUser() {
        binding.btnRegister.setOnClickListener {
            validateAndRegister()
        }
    }

    private fun handleActionDone() {
        binding.inputPassword.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                validateAndRegister()
                return@setOnEditorActionListener true
            }
            false
        }
    }

    private fun validateAndRegister() {
        if (TextUtils.isEmpty(binding.inputUserName.text.toString()) || TextUtils.isEmpty(binding.inputPassword.text.toString())) {
            Toast.makeText(this, getString(R.string.error_all_fields_are_required), Toast.LENGTH_SHORT).show()
        } else if (binding.inputPassword.text.toString().length < 6) {
            binding.inputLayoutPassword.error = getString(R.string.error_password_at_leat_six_characters)
            Toast.makeText(this, getString(R.string.error_password_at_leat_six_characters), Toast.LENGTH_SHORT).show()
        } else {
            hideKeyboard()
            viewModel.register(binding.inputUserName.text.toString(), binding.inputLastName.text.toString(),
                    binding.inputEmail.text.toString(), binding.inputPassword.text.toString())
        }
    }

    private fun observeViewModel() {
        viewModel.liveDataRegisterViewState.observe(this, androidx.lifecycle.Observer {
            when (it) {
                is RegisterViewModel.RegisterViewState.Loading -> showLoading()
                is RegisterViewModel.RegisterViewState.Success -> showSuccessResult()
                is RegisterViewModel.RegisterViewState.Error -> showError()
            }
        })

        viewModel.internetStatus.observe(this, Observer {
            if (!it) {
                this.finish()
                startActivity(NoInternetConnectionActivity.prepareInternet(this))
            }
        })
    }

    private fun showLoading() {
        binding.loader.show()
        binding.widgetGroup.invisible()
        binding.toolbar.invisible()
        binding.btnRegister.invisible()
    }

    private fun showError() {
        binding.loader.gone()
        binding.widgetGroup.show()
        binding.toolbar.show()
        binding.btnRegister.show()
        Toast.makeText(this, getString(R.string.register_error_email_password), Toast.LENGTH_SHORT).show()
    }

    private fun showSuccessResult() {
        val intent = MainActivity.prepareIntent(this)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        finish()
        startActivity(intent)
    }

    private fun initToolbar() {
        setSupportActionBar(binding.toolbar)
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setHomeButtonEnabled(true)
            setDisplayShowTitleEnabled(true)
            title = getString(R.string.register)
            setHomeAsUpIndicator(R.drawable.ic_back)
            binding.toolbar.setNavigationOnClickListener { finish() }
        }
    }

    companion object {
        fun prepareIntent(context: Context) = Intent(context, RegisterActivity::class.java)
    }
}